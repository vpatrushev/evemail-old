package ru.micropint.evemail.content;

class CharactersTable {

	private CharactersTable() {

	}
	
	public static final String TABLE_NAME = "Characters";
	
	public static final String COLUMN_NAME_ID = "character_id";
	public static final String COLUMN_NAME_NAME = "character_name";

}
