package ru.micropint.evemail.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import ru.micropint.evemail.data.IDString;

public class CharacterNamesHandler extends BaseListHandler<IDString> {

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		if (localName.equals("row")) {
			mData.add(new IDString(Integer.parseInt(attributes.getValue("characterID")), attributes.getValue("name")));
		}
	}
	
	
}
