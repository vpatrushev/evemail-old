package ru.micropint.evemail.content;

import ru.micropint.evemail.appengine.Constants;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

public class MailProvider extends ContentProvider {

	private static final String TAG = "MailProvider";

	private static final int MAIL = 1;
	private static final int MAIL_ID = 2;
	private static final int MAIL_NEW = 3;
	private static final int CHARACTER = 21;
	private static final int CHARACTER_ID = 22;

	private static UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

	private DatabaseHelper mHelper;
	private SQLiteDatabase mDatabase;

	static {
		sUriMatcher.addURI(MailContract.AUTHORITY, MailContract.Mail.BASE_PATH, MAIL);
		sUriMatcher.addURI(MailContract.AUTHORITY, MailContract.Mail.BASE_PATH + "/#", MAIL_ID);
		sUriMatcher.addURI(MailContract.AUTHORITY, MailContract.Mail.BASE_PATH + MailContract.Mail.NEW_PATH, MAIL_NEW);
		sUriMatcher.addURI(MailContract.AUTHORITY, MailContract.Character.BASE_PATH, CHARACTER);
		sUriMatcher.addURI(MailContract.AUTHORITY, MailContract.Character.BASE_PATH + "/#", CHARACTER_ID);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		createDatabase();

		switch (sUriMatcher.match(uri)) {
			case MAIL:
				break;
			case MAIL_ID:
				selection = BaseColumns._ID + "=" + uri.getLastPathSegment();
				break;
			default:
				throw new IllegalArgumentException("Unknown or illegal URI");
		}
		ContentValues values = new ContentValues();
		values.put(MailTable.COLUMN_NAME_IS_DELETED, true);
		int rowsAffected = mDatabase.update(MailTable.TABLE_NAME, values, selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		switch (sUriMatcher.match(uri)) {
			case MAIL:
				return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.ru.micropint.evemail.mail";
			case MAIL_ID:
				return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.ru.micropint.evemail.mail";
			case CHARACTER:
				return ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.ru.micropint.evemail.character";
			case CHARACTER_ID:
				return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.ru.micropint.evemail.character";
			default:
				throw new IllegalArgumentException("Unknown URI");
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Log.d(TAG, "Inserting to " + uri.toString());
		Log.d(TAG, "ID = " + values.getAsString(BaseColumns._ID));

		createDatabase();
		long id;

		switch (sUriMatcher.match(uri)) {
			case MAIL:
				// updateIdColumnName(values, MailTable.COLUMN_NAME_ID);
				values.put(MailTable.COLUMN_NAME_IS_READ, false);
				values.put(MailTable.COLUMN_NAME_IS_DELETED, false);
				values.put(MailTable.COLUMN_NAME_IS_NEW, 1);
				id = mDatabase.insertWithOnConflict(MailTable.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
				getContext().getContentResolver().notifyChange(uri, null);
				return ContentUris.withAppendedId(MailContract.Character.CONTENT_URI, id);
			case CHARACTER:
				updateIdColumnName(values, CharactersTable.COLUMN_NAME_ID);
				id = mDatabase.insertWithOnConflict(CharactersTable.TABLE_NAME, null, values,
						SQLiteDatabase.CONFLICT_IGNORE);
				getContext().getContentResolver().notifyChange(uri, null);
				return ContentUris.withAppendedId(MailContract.Character.CONTENT_URI, id);
			default:
				throw new IllegalArgumentException("Unknown URI");
		}
	}

	@Override
	public boolean onCreate() {
		mHelper = new DatabaseHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		Log.d(TAG, "Query for " + uri.toString());

		createDatabase();

		SQLiteQueryBuilder query = new SQLiteQueryBuilder();
		int uriType = sUriMatcher.match(uri);

		switch (uriType) {
			case MAIL:
			case MAIL_ID:
			case MAIL_NEW:
				query.setTables(MailTable.TABLE_NAME + " JOIN " + CharactersTable.TABLE_NAME + " ON ("
						+ MailTable.COLUMN_NAME_SENDER_ID + "=" + CharactersTable.COLUMN_NAME_ID + ")");
				// decorateSpecialField(projection, BaseColumns._ID,
				// MailTable.COLUMN_NAME_ID);
				break;
			case CHARACTER:
			case CHARACTER_ID:
				query.setTables(CharactersTable.TABLE_NAME);
				decorateSpecialField(projection, BaseColumns._ID, CharactersTable.COLUMN_NAME_ID);
				break;
			default:
				throw new IllegalArgumentException("Unknown URI");
		}

		switch (uriType) {
			case MAIL:
				if (uri.getQueryParameter(MailContract.Mail.FORCE_ALL) == null) {
					query.appendWhere(MailTable.COLUMN_NAME_IS_DELETED + "=0");
				}
				break;
			case MAIL_ID:
				query.appendWhere(BaseColumns._ID + "=" + uri.getLastPathSegment());
				break;
			case MAIL_NEW:
				query.appendWhere(MailTable.COLUMN_NAME_IS_NEW + "=1");
				break;
			case CHARACTER_ID:
				query.appendWhere(CharactersTable.COLUMN_NAME_ID + "=" + uri.getLastPathSegment());
				break;
		}

		if (uriType == MAIL) {
			resetNewMessages();
		}

		String groupBy = uri.getQueryParameter(Constants.CONTENT_GROUP_BY);
		String having = uri.getQueryParameter(Constants.CONTENT_HAVING);

		decorateSpecialField(projection, BaseColumns._COUNT, "COUNT(1)");

		Cursor cursor = query.query(mDatabase, projection, selection, selectionArgs, groupBy, having, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	private void resetNewMessages() {
		ContentValues values = new ContentValues();
		values.put(MailTable.COLUMN_NAME_IS_NEW, 0);
		mDatabase.update(MailTable.TABLE_NAME, values, MailTable.COLUMN_NAME_IS_NEW + "=1", null);
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		createDatabase();

		switch (sUriMatcher.match(uri)) {
			case MAIL:
				break;
			case MAIL_ID:
				selection = BaseColumns._ID + "=" + uri.getLastPathSegment();
				break;
			default:
				throw new IllegalArgumentException("Unknown or illegal URI");
		}
		int rowsAffected = mDatabase.update(MailTable.TABLE_NAME, values, selection, selectionArgs);
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	private void createDatabase() {
		if (mDatabase == null) {
			Log.d("MailProvider", "Database created");
			mDatabase = mHelper.getWritableDatabase();
		}
	}

	private void decorateSpecialField(String[] projection, String projectionColumnName, String dbColumnName) {
		for (int i = 0; i < projection.length; i++) {
			if (projection[i].equals(projectionColumnName)) {
				projection[i] = dbColumnName + " AS " + projectionColumnName;
			}
		}
	}

	private void updateIdColumnName(ContentValues values, String columnName) {
		if (values.containsKey(BaseColumns._ID)) {
			int id = values.getAsInteger(BaseColumns._ID);
			values.remove(BaseColumns._ID);
			values.put(columnName, id);
		}
	}

}
