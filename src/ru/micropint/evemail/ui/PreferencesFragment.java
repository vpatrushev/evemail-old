package ru.micropint.evemail.ui;

import android.app.Activity;
import android.os.Bundle;
import android.preference.PreferenceFragment;

public class PreferencesFragment extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		int res = getResources().getIdentifier(getArguments().getString("resource"), "xml",
		        getActivity().getPackageName());
		addPreferencesFromResource(res);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((PreferencesActivity)getActivity()).setOptionsMenuVisible(false);
	}
}
