package ru.micropint.evemail.adapters;

import android.content.Context;
import android.view.LayoutInflater;

public abstract class BaseAdapter extends android.widget.BaseAdapter {

	protected LayoutInflater mInflater;

	public BaseAdapter(Context context) {
		mInflater = LayoutInflater.from(context);
	}

}
