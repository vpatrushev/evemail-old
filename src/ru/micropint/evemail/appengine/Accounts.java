package ru.micropint.evemail.appengine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Collection;

import ru.micropint.evemail.EveMail;
import ru.micropint.evemail.data.EveAccount;

import android.content.Context;

public class Accounts extends ArrayList<EveAccount> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6597934527320719820L;

	private static final String STORAGE_NAME = "Accounts";
	
	@Override
	public boolean add(EveAccount object) {
		boolean result = super.add(object);
		save();
		return result;
	}


	@Override
	public void add(int index, EveAccount object) {
		super.add(index, object);
		save();
	}


	@Override
	public boolean addAll(Collection<? extends EveAccount> collection) {
		boolean result = super.addAll(collection);
		save();
		return result;
	}


	@Override
	public boolean addAll(int index, Collection<? extends EveAccount> collection) {
		boolean result = super.addAll(index, collection);
		save();
		return result;
	}


	@Override
	public void clear() {
		super.clear();
		save();
	}


	@Override
	public EveAccount remove(int index) {
		EveAccount result = super.remove(index);
		save();
		return result;
	}


	@Override
	public boolean remove(Object object) {
		boolean result = super.remove(object);
		save();
		return result;
	}


	@Override
	protected void removeRange(int fromIndex, int toIndex) {
		super.removeRange(fromIndex, toIndex);
		save();
	}

	public boolean hasAccount(int userId) {
		for (EveAccount account : this) {
			if (account.getCharacter().getId() == userId) {
				return true;
			}
		}
		return false;
	}

	private void save() {
		FileOutputStream fos;
		try {
			fos = EveMail.getInstance().openFileOutput(STORAGE_NAME, Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(this);
			os.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
	}

	public static Accounts load() {
		try {
			FileInputStream fis = EveMail.getInstance().openFileInput(STORAGE_NAME);
			ObjectInputStream is = new ObjectInputStream(fis);
			Accounts result = (Accounts) is.readObject();
			is.close();
			return result;
		} catch (FileNotFoundException e) {
		} catch (StreamCorruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return new Accounts();
	}

}
