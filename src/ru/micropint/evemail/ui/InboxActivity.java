package ru.micropint.evemail.ui;

import java.util.ArrayList;
import java.util.List;

import ru.micropint.evemail.R;
import ru.micropint.evemail.adapters.InboxAdapter;
import ru.micropint.evemail.adapters.InboxAdapter.ViewHolder;
import ru.micropint.evemail.adapters.InboxNavigationAdapter;
import ru.micropint.evemail.appengine.Constants;
import ru.micropint.evemail.appengine.MailProviderUtils;
import ru.micropint.evemail.appengine.Utils;
import ru.micropint.evemail.content.MailContract;
import ru.micropint.evemail.data.EveAccount;
import ru.micropint.evemail.data.EveCharacter;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.OnAccountsUpdateListener;
import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.ListActivity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CursorAdapter;

public class InboxActivity extends ListActivity implements LoaderCallbacks<Cursor>, OnAccountsUpdateListener,
		OnNavigationListener {

	public static final String EXTRA_CHARACTER_ID = "Account";

	private static final String KEY_LAST_ACCOUNT = "Inbox.LastAccount";

	private static final int MAIL_LOADER_ID = 1;
	private static final int LOADER_UNREAD_COUNT = 2;

	private List<EveAccount> mAccounts;
	private EveAccount mSelectedAccount;
	private InboxNavigationAdapter mNavigationAdapter;
	private InboxAdapter mListAdapter;
	private MailProviderUtils mMailWrapper;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AccountManager manager = AccountManager.get(this);
		manager.addOnAccountsUpdatedListener(this, null, false);
		mAccounts = getAccounts(manager.getAccountsByType(Constants.ACCOUNT_TYPE), manager);

		if (mAccounts.size() == 0) {
			startActivity(new Intent(this, AddAccountActivity.class));
			finish();
		} else {
			setContentView(R.layout.inbox);

			mMailWrapper = new MailProviderUtils(getContentResolver());

			ActionBar actionBar = getActionBar();
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
			actionBar.setDisplayShowTitleEnabled(false);
			setNavigationAdapter(actionBar);

			mListAdapter = new InboxAdapter(this, getListView());
			setListAdapter(mListAdapter);
			getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
			getListView().setMultiChoiceModeListener(list_choice_listener);

			if (getIntent().hasExtra(EXTRA_CHARACTER_ID)) {
				selectCharacter(getIntent().getIntExtra(EXTRA_CHARACTER_ID, 0));
			} else {
				selectLastViewedAccount();
			}
			registerForContextMenu(getListView());
			getListView().setOnItemClickListener(mail_Click);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
		AccountManager manager = AccountManager.get(this);
		manager.removeOnAccountsUpdatedListener(this);
	};

	@Override
	protected void onStart() {
		super.onStart();
		IntentFilter filter = new IntentFilter(Constants.ACTION_NEW_MESSAGE);
		filter.setPriority(2);
		registerReceiver(mNewMessageReceiver, filter);
	};

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver(mNewMessageReceiver);
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Constants.REQUEST_PREFERENCES) {
			InboxAdapter adapter = (InboxAdapter) getListView().getAdapter();
			adapter.updateConvertTimePreference();
			adapter.notifyDataSetChanged();
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.inbox_menu, menu);
		menu.findItem(R.id.DeleteAll).setVisible(Utils.isDevMode());
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.Refresh:
				ContentResolver.requestSync(mSelectedAccount.getAccount(), MailContract.AUTHORITY, new Bundle());
				return true;
			case R.id.MarkAllAsRead:
				mMailWrapper.markAllMessagesRead(mSelectedAccount.getCharacter().getId());
				return true;
			case R.id.Settings:
				startActivityForResult(new Intent(this, PreferencesActivity.class), Constants.REQUEST_PREFERENCES);
				return true;
			case R.id.DeleteAll:
				mMailWrapper.clean(mSelectedAccount.getCharacter().getId());
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		if (v == getListView()) {
			Cursor cursor = ((CursorAdapter) getListAdapter()).getCursor();

			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.inbox_context, menu);

			menu.setHeaderTitle(cursor.getString(cursor.getColumnIndexOrThrow(MailContract.Mail.TITLE)));
			if (cursor.getInt(cursor.getColumnIndexOrThrow(MailContract.Mail.IS_READ)) == 0) {
				menu.findItem(R.id.MarkAsUnread).setVisible(false);
			} else {
				menu.findItem(R.id.MarkAsRead).setVisible(false);
			}
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		Cursor cursor = mListAdapter.getCursor();
		int selectedMessageId = cursor.getInt(cursor.getColumnIndexOrThrow(MailContract.Mail._ID));
		switch (item.getItemId()) {
			case R.id.Delete:
				mMailWrapper.delete(selectedMessageId);
				return true;
			case R.id.MarkAsRead:
				mMailWrapper.markMessageRead(selectedMessageId);
				return true;
			case R.id.MarkAsUnread:
				mMailWrapper.markMessageUnread(selectedMessageId);
				return true;
			default:
				return super.onContextItemSelected(item);
		}
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		mSelectedAccount = mNavigationAdapter.getItem(itemPosition);
		getLoaderManager().restartLoader(MAIL_LOADER_ID, null, this);
		saveLastViewedAccount(mSelectedAccount);
		return true;
	}

	@Override
	public void onAccountsUpdated(Account[] accounts) {
		AccountManager manager = AccountManager.get(this);
		mAccounts = getAccounts(accounts, manager);
		setNavigationAdapter(getActionBar());
	}

	private List<EveAccount> getAccounts(Account[] accounts, AccountManager manager) {
		ArrayList<EveAccount> result = new ArrayList<EveAccount>(accounts.length);
		for (Account account : accounts) {
			if (account.type.equals(Constants.ACCOUNT_TYPE)) {
				EveAccount eveAccount = new EveAccount(manager, account);
				result.add(eveAccount);
			}
		}
		return result;
	}

	private void setNavigationAdapter(ActionBar actionBar) {
		mNavigationAdapter = new InboxNavigationAdapter(actionBar.getThemedContext(), mAccounts,
				getLoaderManager(), LOADER_UNREAD_COUNT);
		actionBar.setListNavigationCallbacks(mNavigationAdapter, this);
		if (mSelectedAccount != null) {
			selectCharacter(mSelectedAccount.getCharacter().getId());
		}
	}

	private void selectLastViewedAccount() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		int accountId = prefs.getInt(KEY_LAST_ACCOUNT, -1);
		selectCharacter(accountId);
	}

	private void selectCharacter(int accountId) {
		for (int i = 0; i < mAccounts.size(); i++) {
			if (mAccounts.get(i).getCharacter().getId() == accountId) {
				getActionBar().setSelectedNavigationItem(i);
				return;
			}
		}
	}

	private void saveLastViewedAccount(EveAccount account) {
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
		editor.putInt(KEY_LAST_ACCOUNT, account.getCharacter().getId());
		editor.commit();
	}

	private OnItemClickListener mail_Click = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, final long id) {
			Intent intent = new Intent(InboxActivity.this, ReadMailActivity.class);
			intent.putExtra(ReadMailActivity.EXTRA_MAIL_ID, id);
			intent.putExtra(ReadMailActivity.EXTRA_CHARACTER_ID, mSelectedAccount.getCharacter().getId());
			startActivity(intent);
		}
	};

	private BroadcastReceiver mNewMessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			EveCharacter character = (EveCharacter) intent.getSerializableExtra(Constants.EXTRA_CHARACTER);
			if (character.getId() == mSelectedAccount.getCharacter().getId()) {
				abortBroadcast();
			}
		}
	};

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		String[] projection;
		CursorLoader loader;
		switch (id) {
			case MAIL_LOADER_ID:
				projection = new String[] {
						MailContract.Mail._ID,
						MailContract.Mail.TITLE,
						MailContract.Mail.SENT_DATE,
						MailContract.Mail.SENDER_NAME,
						MailContract.Mail.IS_READ };
				String characterId = Integer.toString(mSelectedAccount.getCharacter().getId());
				loader = new CursorLoader(
						this, 
						MailContract.Mail.CONTENT_URI, 
						projection, 
						MailContract.Mail.ACCOUNT_ID + "=? AND " + MailContract.Mail.SENDER_ID + " !=?", 
						new String[] { characterId, characterId },
						MailContract.Mail.SENT_DATE + " DESC");
				return loader;
			default:
				return null;
		}

	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		switch (loader.getId()) {
			case MAIL_LOADER_ID:
				mListAdapter.swapCursor(cursor);
				break;
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		switch (loader.getId()) {
			case MAIL_LOADER_ID:
				mListAdapter.swapCursor(null);
				break;
		}
	}

	private MultiChoiceModeListener list_choice_listener = new MultiChoiceModeListener() {

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {

		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.inbox_context, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			long ids[] = getListView().getCheckedItemIds();
			switch (item.getItemId()) {
				case R.id.MarkAsRead:
					for (int i = 0; i < ids.length; i++) {
						mMailWrapper.markMessageRead(ids[i]);
					}
					return true;
				case R.id.MarkAsUnread:
					for (int i = 0; i < ids.length; i++) {
						mMailWrapper.markMessageUnread(ids[i]);
					}
					return true;
				case R.id.Delete:
					for (int i = 0; i < ids.length; i++) {
						mMailWrapper.delete(ids[i]);
					}
					return true;
				default:
					return false;
			}
		}

		@Override
		public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
			ListView listView = getListView();
			View listItem = listView.getChildAt(position - listView.getFirstVisiblePosition());
			InboxAdapter.ViewHolder holder = (ViewHolder) listItem.getTag();
			holder.checkbox.setChecked(checked);

			mode.setTitle(getString(R.string.inbox_ActionModeTitle, getListView().getCheckedItemCount()));
		}
	};

}