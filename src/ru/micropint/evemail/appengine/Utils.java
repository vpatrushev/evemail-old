package ru.micropint.evemail.appengine;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import ru.micropint.evemail.EveMail;
import ru.micropint.evemail.R;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public final class Utils {

	private static final SimpleDateFormat mEveDateFormat;

	static {
		mEveDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
		mEveDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	/**
	 * ��������� ������ � ������������ � density ������
	 * 
	 * @param size
	 *            ������ � dp
	 * @return ������ � ��������
	 */
	public int convertToDp(int size, Context context) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, size, context.getResources()
				.getDisplayMetrics());
	}

	/**
	 * ������� �������� ����������
	 * 
	 * @param context
	 *            ��������, � ������� ������� ����������
	 * @param edit
	 *            ���� ��������������, �������� ����������� ����������
	 */
	public static void hideKeyboard(Context context, EditText edit) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
	}

	/**
	 * ����������� ������ ����, ������������ Eve API, � ����. ������������ UTC
	 * ��������� ����
	 * 
	 * @param value
	 *            ������ � �����
	 * @return ����
	 */
	public static Date parseDate(String value) {
		try {
			return mEveDateFormat.parse(value);
		} catch (ParseException e) {
			return new Date(0);
		}
	}

	public static void setTimezone(DateFormat dateFormat) {
		String convertTimePrefName = EveMail.getInstance().getString(R.string.prefKey_ConvertToLocalTime);
		boolean convertTime = PreferenceManager.getDefaultSharedPreferences(EveMail.getInstance()).getBoolean(
				convertTimePrefName, false);
		setTimezone(dateFormat, convertTime);
	}

	public static void setTimezone(Calendar calendar) {
		String convertTimePrefName = EveMail.getInstance().getString(R.string.prefKey_ConvertToLocalTime);
		boolean convertTime = PreferenceManager.getDefaultSharedPreferences(EveMail.getInstance()).getBoolean(
				convertTimePrefName, false);
		setTimezone(calendar, convertTime);
	}

	public static void setTimezone(DateFormat dateFormat, boolean convertTime) {
		if (convertTime) {
			dateFormat.setTimeZone(TimeZone.getDefault());
		} else {
			dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		}
	}

	public static void setTimezone(Calendar calendar, boolean convertTime) {
		if (convertTime) {
			calendar.setTimeZone(TimeZone.getDefault());
		} else {
			calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
		}
	}

	public static boolean isDevMode() {
		Context c = EveMail.getInstance();
		try {
			PackageInfo info = c.getPackageManager().getPackageInfo(c.getPackageName(), 0);
			return (info.applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
		} catch (NameNotFoundException e) {
			return false;
		}
	}

	public static boolean isDefaultPreferenceExists(int prefKeyId) {
		String key = EveMail.getInstance().getString(prefKeyId);
		return PreferenceManager.getDefaultSharedPreferences(EveMail.getInstance()).contains(key);
	}

	public static String getDefaultPreferenceString(int prefKeyId, String defValue) {
		String key = EveMail.getInstance().getString(prefKeyId);
		return PreferenceManager.getDefaultSharedPreferences(EveMail.getInstance()).getString(key, defValue);
	}

	public static int getDefaultPreferenceInt(int prefKeyId, int defValue) {
		String key = EveMail.getInstance().getString(prefKeyId);
		return PreferenceManager.getDefaultSharedPreferences(EveMail.getInstance()).getInt(key, defValue);
	}

	public static boolean getDefaultPreferenceBoolean(int prefKeyId, boolean defValue) {
		String key = EveMail.getInstance().getString(prefKeyId);
		return PreferenceManager.getDefaultSharedPreferences(EveMail.getInstance()).getBoolean(key, defValue);
	}

	public static String readFile(String fileName) {
		FileInputStream fis;
		try {
			fis = EveMail.getInstance().openFileInput(fileName);
			BufferedReader r = new BufferedReader(new InputStreamReader(fis));
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) {
				total.append(line);
			}
			return total.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "";
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String getPortraitFileName(int characterId) {
		return EveMail.getInstance().getCacheDir() + "/" + characterId + ".jpg";
	}
	
	public static Bitmap downloadPortrait(int characterId) {
		Log.d(Utils.class.getName(), "Downloading portrait " + characterId);
		String url = Constants.IMAGE_SERVER_URL + "Character/" + characterId + "_200.jpg";
		Bitmap portrait = Downloader.downloadBitmap(url);
		Bitmap scaledPortrait = null;
		if (portrait != null) {
			int portraitSize = EveMail.getInstance().getResources().getDimensionPixelSize(R.dimen.notification_height);
			scaledPortrait = Bitmap.createScaledBitmap(portrait, portraitSize,
					portraitSize, false);
			FileOutputStream stream;
			try {
				stream = new FileOutputStream(Utils.getPortraitFileName(characterId));
				scaledPortrait.compress(CompressFormat.JPEG, 85, stream);
				stream.flush();
				stream.close();
			} catch (FileNotFoundException e) {
				Log.w(Utils.class.getName(), "Error saving portrait " + characterId);
			} catch (IOException e) {
				Log.w(Utils.class.getName(), "Error saving portrait " + characterId);
			}
		}
		return scaledPortrait;
	}

	private Utils() {

	}
}
