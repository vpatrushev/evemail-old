package ru.micropint.evemail.appengine;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MailSyncAdapterService extends Service {
	
	private static final Object LOCK = new Object();
	private static MailSyncAdapter sSyncAdapter;

	@Override
	public void onCreate() {
		synchronized (LOCK) {
			if (sSyncAdapter == null) {
				sSyncAdapter = new MailSyncAdapter(getApplicationContext(), true);
			}
		}
	};
	
	@Override
	public IBinder onBind(Intent intent) {
		return sSyncAdapter.getSyncAdapterBinder();
	}

}
