package ru.micropint.evemail.content;

import java.util.List;

import ru.micropint.evemail.data.IDString;
import ru.micropint.evemail.data.RawMail;
import ru.micropint.evemail.data.ViewableMail;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

class MailDatabase {

	static final String TAG = "MailDatabase";

	private DatabaseHelper mDatabaseHelper;
	private SQLiteDatabase mDatabase;

	public MailDatabase(Context context) {
		mDatabaseHelper = new DatabaseHelper(context);
		mDatabase = mDatabaseHelper.getWritableDatabase();
	}

	public void insertMail(List<RawMail> data, int characterId, boolean markAsNew) {
		mDatabase.beginTransaction();
		for (RawMail mail : data) {
			ContentValues values = new ContentValues();
			values.put(MailTable.COLUMN_NAME_ID, mail.getId());
			values.put(MailTable.COLUMN_NAME_TITLE, mail.getTitle());
			values.put(MailTable.COLUMN_NAME_BODY, mail.getBody());
			values.put(MailTable.COLUMN_NAME_SENDER_ID, mail.getSenderId());
			values.put(MailTable.COLUMN_NAME_SENT_DATE, mail.getSentDate().getTime());
			values.put(MailTable.COLUMN_NAME_TO_CORP_OR_ALLIANCE_ID, mail.getCorpOrAllianceId());
			values.put(MailTable.COLUMN_NAME_ACCOUNT_ID, characterId);
			values.put(MailTable.COLUMN_NAME_IS_READ, false);
			values.put(MailTable.COLUMN_NAME_IS_DELETED, false);
			values.put(MailTable.COLUMN_NAME_IS_NEW, markAsNew);
			long result = mDatabase.insertWithOnConflict(MailTable.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
			Log.d(TAG, "Inserted mail " + result);
		}
		mDatabase.setTransactionSuccessful();
		mDatabase.endTransaction();
	}

	public Cursor getMailHeaders(int characterId) {
		String query = "SELECT " + MailTable.COLUMN_NAME_ID + " _id,"
				+ MailTable.COLUMN_NAME_ID + ","
				+ MailTable.COLUMN_NAME_TITLE + ","
				+ MailTable.COLUMN_NAME_SENDER_ID + ","
				+ MailTable.COLUMN_NAME_SENT_DATE + ","
				+ MailTable.COLUMN_NAME_TO_CORP_OR_ALLIANCE_ID + ","
				+ MailTable.COLUMN_NAME_IS_READ + ","
				+ CharactersTable.COLUMN_NAME_NAME
				+ " FROM " + MailTable.TABLE_NAME
				+ " LEFT JOIN " + CharactersTable.TABLE_NAME
				+ " ON " + MailTable.TABLE_NAME + "." + MailTable.COLUMN_NAME_SENDER_ID + "=" + CharactersTable.TABLE_NAME + "." + CharactersTable.COLUMN_NAME_ID
				+ " WHERE " + MailTable.COLUMN_NAME_ACCOUNT_ID + "=" + characterId
				+ " AND " + MailTable.COLUMN_NAME_IS_DELETED + "=0"
				+ " AND " + MailTable.COLUMN_NAME_SENDER_ID + "!=" + characterId
				+ " ORDER BY " + MailTable.COLUMN_NAME_SENT_DATE + " DESC";
		Log.d(TAG, query);
		return mDatabase.rawQuery(query, null);
	}

	public boolean isMailExists(int mailId) {
		Cursor cur = mDatabase.query(MailTable.TABLE_NAME, new String[] { MailTable.COLUMN_NAME_ID },
				MailTable.COLUMN_NAME_ID + "=?", new String[] { Integer.toString(mailId) }, null, null, null, "1");
		boolean result = cur.getCount() > 0;
		cur.close();
		return result;
	}

	public boolean isCharacterExists(int characterId) {
		Cursor cur = mDatabase.query(CharactersTable.TABLE_NAME, new String[] { CharactersTable.COLUMN_NAME_ID },
				CharactersTable.COLUMN_NAME_ID + "=?", new String[] { Integer.toString(characterId) }, null, null, null, "1");
		boolean result = cur.getCount() > 0;
		cur.close();
		return result;
	}

	public void addCharacters(List<IDString> data) {
		mDatabase.beginTransaction();
		for (IDString character : data) {
			ContentValues values = new ContentValues();
			values.put(CharactersTable.COLUMN_NAME_ID, character.getId());
			values.put(CharactersTable.COLUMN_NAME_NAME, character.getName());
			long result = mDatabase.insertWithOnConflict(CharactersTable.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
			Log.d(TAG, "Inserted character " + result);
		}
		mDatabase.setTransactionSuccessful();
		mDatabase.endTransaction();
	}

	public ViewableMail getMail(int mailId) {
		String query = "SELECT " + MailTable.COLUMN_NAME_TITLE + ","
				+ MailTable.COLUMN_NAME_BODY + ","
				+ MailTable.COLUMN_NAME_SENDER_ID + ","
				+ CharactersTable.COLUMN_NAME_NAME + ","
				+ MailTable.COLUMN_NAME_SENT_DATE + ","
				+ MailTable.COLUMN_NAME_ACCOUNT_ID
				+ " FROM " + MailTable.TABLE_NAME
				+ " LEFT JOIN " + CharactersTable.TABLE_NAME
				+ " ON " + MailTable.TABLE_NAME + "." + MailTable.COLUMN_NAME_SENDER_ID + "=" + CharactersTable.TABLE_NAME + "." + CharactersTable.COLUMN_NAME_ID
				+ " WHERE " + MailTable.COLUMN_NAME_ID + "=?";
		Cursor cur = mDatabase.rawQuery(query, new String[] { Integer.toString(mailId) });
		cur.moveToFirst();
		ViewableMail result = new ViewableMail();
		result.setId(mailId);
		result.setTitle(cur.getString(cur.getColumnIndexOrThrow(MailTable.COLUMN_NAME_TITLE)));
		result.setBody(cur.getString(cur.getColumnIndexOrThrow(MailTable.COLUMN_NAME_BODY)));
		result.setSenderId(cur.getInt(cur.getColumnIndexOrThrow(MailTable.COLUMN_NAME_SENDER_ID)));
		result.setRecipientId(cur.getInt(cur.getColumnIndexOrThrow(MailTable.COLUMN_NAME_ACCOUNT_ID)));
		result.setSenderName(cur.getString(cur.getColumnIndexOrThrow(CharactersTable.COLUMN_NAME_NAME)));
		result.setUnixSentDate(cur.getLong(cur.getColumnIndexOrThrow(MailTable.COLUMN_NAME_SENT_DATE)));
		cur.close();
		return result;
	}

	public void setMailReadState(int mailId, boolean isRead) {
		ContentValues values = new ContentValues();
		values.put(MailTable.COLUMN_NAME_IS_READ, isRead);
		mDatabase.update(MailTable.TABLE_NAME, values, MailTable.COLUMN_NAME_ID + "=?", new String[] { Integer.toString(mailId) });
	}

	public void deleteMail(int mailId) {
		ContentValues values = new ContentValues();
		values.put(MailTable.COLUMN_NAME_IS_DELETED, true);
		mDatabase.update(MailTable.TABLE_NAME, values, MailTable.COLUMN_NAME_ID + "=?", new String[] { Integer.toString(mailId) });
	}

	public void markAllMessagesRead(int characterId) {
		ContentValues values = new ContentValues();
		values.put(MailTable.COLUMN_NAME_IS_READ, true);
		mDatabase.update(MailTable.TABLE_NAME, values, MailTable.COLUMN_NAME_ACCOUNT_ID + "=?", new String[] { Integer.toString(characterId) });
	}

	public Cursor getNewMail() {
		String query = "SELECT " + MailTable.COLUMN_NAME_ID + ","
				+ MailTable.COLUMN_NAME_TITLE + ","
				+ MailTable.COLUMN_NAME_SENT_DATE + ","
				+ CharactersTable.COLUMN_NAME_NAME
				+ " FROM " + MailTable.TABLE_NAME
				+ " LEFT JOIN " + CharactersTable.TABLE_NAME
				+ " ON " + MailTable.TABLE_NAME + "." + MailTable.COLUMN_NAME_SENDER_ID + "=" + CharactersTable.TABLE_NAME + "." + CharactersTable.COLUMN_NAME_ID
				+ " WHERE " + MailTable.COLUMN_NAME_IS_NEW + "=1"
				+ " AND " + MailTable.COLUMN_NAME_SENDER_ID + "!=" + MailTable.COLUMN_NAME_ACCOUNT_ID
				+ " ORDER BY " + MailTable.COLUMN_NAME_SENT_DATE + " DESC";
		Log.d(TAG, query);
		return mDatabase.rawQuery(query, null);
	}

	public Cursor getNewMail(int characterId) {
		String query = "SELECT " + MailTable.COLUMN_NAME_ID + ","
				+ MailTable.COLUMN_NAME_TITLE + ","
				+ MailTable.COLUMN_NAME_SENT_DATE + ","
				+ CharactersTable.COLUMN_NAME_NAME
				+ " FROM " + MailTable.TABLE_NAME
				+ " LEFT JOIN " + CharactersTable.TABLE_NAME
				+ " ON " + MailTable.TABLE_NAME + "." + MailTable.COLUMN_NAME_SENDER_ID + "=" + CharactersTable.TABLE_NAME + "." + CharactersTable.COLUMN_NAME_ID
				+ " WHERE " + MailTable.COLUMN_NAME_ACCOUNT_ID + "=" + characterId
				+ " AND " + MailTable.COLUMN_NAME_IS_NEW + "=1"
				+ " AND " + MailTable.COLUMN_NAME_SENDER_ID + "!=" + characterId
				+ " ORDER BY " + MailTable.COLUMN_NAME_SENT_DATE + " DESC";
		Log.d(TAG, query);
		return mDatabase.rawQuery(query, null);
	}

	public void markAllMessagesOld(int characterId) {
		ContentValues values = new ContentValues();
		values.put(MailTable.COLUMN_NAME_IS_NEW, false);
		mDatabase.update(MailTable.TABLE_NAME, values, MailTable.COLUMN_NAME_ACCOUNT_ID + "=?", new String[] { Integer.toString(characterId) });
	}

	public int getUnreadCount(int characterId) {
		Cursor c = mDatabase.query(
				MailTable.TABLE_NAME,
				new String[] { MailTable.COLUMN_NAME_ID },
				MailTable.COLUMN_NAME_IS_READ + "=0 AND " +
						MailTable.COLUMN_NAME_SENDER_ID + "!=" + characterId + " AND " +
						MailTable.COLUMN_NAME_IS_DELETED + "=0",
				null, null, null, null);
		int result = c.getCount();
		c.close();
		return result;
	}

	public void clean() {
		mDatabase.delete(MailTable.TABLE_NAME, null, null);
	}

}
