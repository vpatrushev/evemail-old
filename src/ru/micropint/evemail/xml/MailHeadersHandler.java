package ru.micropint.evemail.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import ru.micropint.evemail.appengine.Constants;
import ru.micropint.evemail.appengine.Utils;
import ru.micropint.evemail.data.RawMail;
import android.text.Html;

public class MailHeadersHandler extends BaseListHandler<RawMail> {
	
	private long cachedUntil;
	private long currentTime;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		if (localName.equals("row")) {
			RawMail header = new RawMail();
			header.setId(Integer.parseInt(attributes.getValue("messageID")));
			header.setSenderId(Integer.parseInt(attributes.getValue("senderID")));
			header.setSentDate(Utils.parseDate(attributes.getValue("sentDate")));
			header.setTitle(Html.fromHtml(attributes.getValue("title")).toString());
			if (attributes.getValue("toCorpOrAllianceID").length() > 0) {
				header.setCorpOrAllianceId(Integer.parseInt(attributes.getValue("toCorpOrAllianceID")));
			} else {
				header.setCorpOrAllianceId(Constants.NULL_INT);
			}
			if (attributes.getValue("toCharacterIDs").length() > 0) {
				header.setCharacterId(attributes.getValue("toCharacterIDs"));				
			}
			if (attributes.getValue("toListID").length() > 0) {
				header.setListId(Integer.parseInt(attributes.getValue("toListID")));
			} else {
				header.setListId(Constants.NULL_INT);
			}
			mData.add(header);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("cachedUntil")) {
			cachedUntil = Utils.parseDate(mTagText).getTime();
		} else if (localName.equals("currentTime")) {
			currentTime = Utils.parseDate(mTagText).getTime();
		}
	}

	public long getCachedUntil() {
		return cachedUntil;
	}

	public long getCurrentTime() {
		return currentTime;
	}

}
