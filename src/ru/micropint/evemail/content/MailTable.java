package ru.micropint.evemail.content;

final class MailTable {
	
	private MailTable() {
		
	}
	
	public static final String TABLE_NAME = "Mail";
	
	public static final String COLUMN_NAME_ID = "header_id";
	public static final String COLUMN_NAME_SENDER_ID = "sender_id";
	public static final String COLUMN_NAME_SENT_DATE = "sent_date";
	public static final String COLUMN_NAME_TITLE = "title";
	public static final String COLUMN_NAME_BODY = "body";
	public static final String COLUMN_NAME_TO_CORP_OR_ALLIANCE_ID = "to_corp_alliance_id";
	public static final String COLUMN_NAME_TO_CHARACTER_IDS = "to_character_ids";
	public static final String COLUMN_NAME_TO_LIST_ID = "to_list_id";
	public static final String COLUMN_NAME_ACCOUNT_ID = "account_id";
	public static final String COLUMN_NAME_IS_READ = "is_read";
	public static final String COLUMN_NAME_IS_DELETED = "is_deleted";
	public static final String COLUMN_NAME_IS_NEW = "is_new";

}
