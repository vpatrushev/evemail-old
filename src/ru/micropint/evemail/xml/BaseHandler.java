package ru.micropint.evemail.xml;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class BaseHandler extends DefaultHandler {
	
	private StringBuilder mBuilder;
	private StringBuilder mDocBuilder;
	private String mError;
	protected String mTagText;

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		super.characters(ch, start, length);
		mBuilder.append(ch, start, length);
		mDocBuilder.append(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		super.endElement(uri, localName, qName);
		mTagText = mBuilder.toString().trim();
		mBuilder.setLength(0);
		if (localName.equals("error")) {
			mError = mTagText;
		}
	}
	
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		mBuilder = new StringBuilder();
		mDocBuilder = new StringBuilder();
	}
	
	public boolean hasError() {
		return mError != null && mError.length() > 0;
	}

	public String getError() {
		return mError;
	}

	public void setError(String error) {
		this.mError = error;
	}
}
