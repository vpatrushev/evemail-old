package ru.micropint.evemail.data;

public class ViewableMail {

	private int id;
	private String title;
	private String body;
	private int senderId;
	private int recipientId;
	private String senderName;
	private long unixSentDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getSenderName() {
		return senderName;
	}
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}
	public long getUnixSentDate() {
		return unixSentDate;
	}
	public void setUnixSentDate(long unixSentDate) {
		this.unixSentDate = unixSentDate;
	}
	public int getSenderId() {
		return senderId;
	}
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	public int getRecipientId() {
		return recipientId;
	}
	public void setRecipientId(int recipientId) {
		this.recipientId = recipientId;
	}	
}
