package ru.micropint.evemail.data;

import java.io.Serializable;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;

public class EveCharacter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3814387678130219689L;
	
	public static final String ID = "EveCharacter.ID";
	public static final String NAME = "EveCharacter.Name";
	public static final String CORP_NAME = "EveCharacter.CorpName";
	
	private int id;
	private String name;
	private String corporationName;
	
	public EveCharacter() {
		
	}
	
	public EveCharacter(AccountManager manager, Account account) {
		id = Integer.parseInt(manager.getUserData(account, ID));
		name = manager.getUserData(account, NAME);
		corporationName = manager.getUserData(account, CORP_NAME);
	}
	
	public void writeToStringBundle(Bundle bundle) {
		bundle.putString(ID, Integer.toString(id));
		bundle.putString(NAME, name);
		bundle.putString(CORP_NAME, corporationName);
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCorporationName() {
		return corporationName;
	}
	public void setCorporationName(String corporationName) {
		this.corporationName = corporationName;
	}
	
}
