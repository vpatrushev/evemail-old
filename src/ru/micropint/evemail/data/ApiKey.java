package ru.micropint.evemail.data;

import java.io.Serializable;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;

public class ApiKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8604780033152011539L;
	
	private static final String KEY_ID = "ApiKey.KeyID";
	private static final String VERIFICATION_CODE = "ApiKEy.VerificationCode";
	
	private String keyId;
	private String verificationCode;

	public ApiKey() {

	}

	public ApiKey(String keyId, String verificationCode) {
		this.keyId = keyId;
		this.verificationCode = verificationCode;
	}
	
	public ApiKey(AccountManager manager, Account account) {
		keyId = manager.getUserData(account, KEY_ID);
		verificationCode = manager.getUserData(account, VERIFICATION_CODE);
	}
	
	public void writeToStringBundle(Bundle bundle) {
		bundle.putString(KEY_ID, keyId);
		bundle.putString(VERIFICATION_CODE, verificationCode);
	}

	public String getKeyId() {
		return keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

}
