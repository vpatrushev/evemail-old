package ru.micropint.evemail.xml;

import java.util.ArrayList;

import org.xml.sax.SAXException;

public class BaseListHandler<T> extends BaseHandler {

	protected ArrayList<T> mData;

	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		
		mData = new ArrayList<T>();
	}
	
	public ArrayList<T> getData() {
		return mData;
	}
}
