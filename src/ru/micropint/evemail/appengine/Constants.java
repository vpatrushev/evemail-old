package ru.micropint.evemail.appengine;


public final class Constants {

	public static final String API_URL = "https://api.eveonline.com/";
	public static final String IMAGE_SERVER_URL = "http://image.eveonline.com/";
	public static final String DEBUG_TAG = "EveMailDebug";
	public static final String SERVER_DEBUG_TAG = "ServerDebug";
	public static final String ACCOUNT_TYPE = "ru.micropint.evemail.account";
	public static final String ACCOUNT_DATA_KEY = "eve_account";
	
	public static final int REQUEST_ADD_ACCOUNT = 1;
	public static final int REQUEST_PREFERENCES = 2;
	
	public static final int NOTIFICATION_API_ERROR = 1;
	public static final int NOTIFICATION_NEW_MAIL = 2;
	
	public static final String CONTENT_GROUP_BY = "group";
	public static final String CONTENT_HAVING = "having";
	
	public static final String ACTION_NEW_MESSAGE = "ru.micropint.evemail.new_message";
	
	public static final String EXTRA_CHARACTER = "Character";
	
	public static final int NULL_INT = -1;
	
	private Constants() {
		
	}
}
