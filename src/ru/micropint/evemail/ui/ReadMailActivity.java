package ru.micropint.evemail.ui;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;

import ru.micropint.evemail.R;
import ru.micropint.evemail.appengine.Constants;
import ru.micropint.evemail.appengine.MailProviderUtils;
import ru.micropint.evemail.appengine.Utils;
import ru.micropint.evemail.content.MailContract;
import ru.micropint.evemail.data.EveCharacter;
import ru.micropint.evemail.data.ViewableMail;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ReadMailActivity extends BaseActivity {

	public static final String EXTRA_MAIL_ID = "MailId";
	public static final String EXTRA_CHARACTER_ID = "CharacterId";

	private static final String HREF_PATTERN = "http://";
	private static final String HREF_END = "</a>";
	private static final DateFormat DATE_FORMAT = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);

	private ImageView mSenderPortrait;
	private TextView mSenderName;
	private TextView mSentDate;
	private TextView mTitle;
	private WebView mBody;

	private long mMailRecordId;
	private int mCharacterId;
	private ViewableMail mMail;
	private boolean mParseLinks;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.read_mail);

		mSenderPortrait = (ImageView) findViewById(R.id.SenderPortrait);
		mSenderName = (TextView) findViewById(R.id.SenderName);
		mSentDate = (TextView) findViewById(R.id.SentDate);
		mTitle = (TextView) findViewById(R.id.Title);
		mBody = (WebView) findViewById(R.id.Body);

		mMailRecordId = getIntent().getLongExtra(EXTRA_MAIL_ID, 0);
		mCharacterId = getIntent().getIntExtra(EXTRA_CHARACTER_ID, 0);
		mParseLinks = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
				getResources().getString(R.string.prefKey_ParseLinks), true);
		Utils.setTimezone(DATE_FORMAT);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		if (loadMail()) {
			showMail();
		} else {
			Toast.makeText(this, R.string.readMail_NoMail, Toast.LENGTH_SHORT).show();
			finish();
		}
		new MailProviderUtils(getContentResolver()).markMessageRead(mMailRecordId);
	}

	@Override
	protected void onStart() {
		super.onStart();
		IntentFilter filter = new IntentFilter(Constants.ACTION_NEW_MESSAGE);
		filter.setPriority(2);
		registerReceiver(mNewMessageReceiver, filter);
	};

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver(mNewMessageReceiver);
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = new MenuInflater(this);
		inflater.inflate(R.menu.read_mail_menu, menu);
		return true;
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				launchHomeActivity();
				return true;
			case R.id.Delete:
				MailProviderUtils mailWrapper = new MailProviderUtils(getContentResolver());
				mailWrapper.delete(mMailRecordId);
				launchHomeActivity();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void launchHomeActivity() {
		Intent intent = new Intent(this, InboxActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	};

	private boolean loadMail() {
		String[] projection = {
				MailContract.Mail.MAIL_ID,
				MailContract.Mail.TITLE,
				MailContract.Mail.SENDER_ID,
				MailContract.Mail.SENDER_NAME,
				MailContract.Mail.BODY,
				MailContract.Mail.SENT_DATE,
				MailContract.Mail.ACCOUNT_ID };
		Cursor cursor = getContentResolver().query(
				ContentUris.withAppendedId(MailContract.Mail.CONTENT_URI, mMailRecordId), 
				projection, null, null, null);
		if (cursor.moveToFirst()) {
			mMail = new ViewableMail();
			mMail.setId(cursor.getInt(cursor.getColumnIndexOrThrow(MailContract.Mail.MAIL_ID)));
			mMail.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(MailContract.Mail.TITLE)));
			mMail.setSenderId(cursor.getInt(cursor.getColumnIndexOrThrow(MailContract.Mail.SENDER_ID)));
			mMail.setSenderName(cursor.getString(cursor.getColumnIndexOrThrow(MailContract.Mail.SENDER_NAME)));
			mMail.setBody(cursor.getString(cursor.getColumnIndexOrThrow(MailContract.Mail.BODY)));
			mMail.setUnixSentDate(cursor.getLong(cursor.getColumnIndexOrThrow(MailContract.Mail.SENT_DATE)));
			mMail.setRecipientId(cursor.getInt(cursor.getColumnIndexOrThrow(MailContract.Mail.ACCOUNT_ID)));
			return true;
		} else {
			return false;
		}
	}

	private void showMail() {
		mSenderName.setText(mMail.getSenderName());
		mSentDate.setText(DATE_FORMAT.format(new Date(mMail.getUnixSentDate())));
		mTitle.setText(mMail.getTitle());
		showPortrait();

		StringBuilder body = new StringBuilder(mMail.getBody());
		if (mParseLinks) {
			addHrefs(body);
		}
		mBody.loadData(formatBody(body.toString()), "text/html; charset=utf-8", null);
		Log.d("Mail", formatBody(body.toString()));
	}

	private void showPortrait() {
		File portraitFile = new File(Utils.getPortraitFileName(mMail.getSenderId()));
		if (portraitFile.exists()) {
			mSenderPortrait.setImageBitmap(BitmapFactory.decodeFile(portraitFile.getAbsolutePath()));
		} else {
			new LoadPortraitTask().execute(mMail.getSenderId());
		}
	}

	private String formatBody(String body) {
		return "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/></head>"
				+ "<style type=\"text/css\">a:link {color: #ffff00; }</style>" + "<body bgcolor=\"#252525\">" + body
				+ "</body></html>";
	}

	private void addHrefs(StringBuilder data) {
		int startIndex = data.indexOf(HREF_PATTERN);
		while (startIndex >= 0) {
			int endIndex = indexOfLinkEnd(data, startIndex + HREF_PATTERN.length());
			String link = "<a href=\"" + data.substring(startIndex, endIndex) + "\">";
			data.insert(startIndex, link);
			data.insert(endIndex + link.length(), HREF_END);
			startIndex = data.indexOf(HREF_PATTERN, endIndex + link.length() + HREF_END.length());
		}
	}

	private int indexOfLinkEnd(StringBuilder data, int startIndex) {
		int result = startIndex;
		while (result < data.length()) {
			if (data.charAt(result) == ' ' || data.charAt(result) == '<') {
				return result;
			}
			result++;
		}
		return data.length();
	}

	private BroadcastReceiver mNewMessageReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			EveCharacter character = (EveCharacter) intent.getSerializableExtra(Constants.EXTRA_CHARACTER);
			if (character.getId() == mCharacterId) {
				abortBroadcast();
			}
		}
	};

	private class LoadPortraitTask extends AsyncTask<Integer, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(Integer... params) {
			int senderId = params[0];
			return Utils.downloadPortrait(senderId);
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			if (!isCancelled() && result != null) {
				mSenderPortrait.setImageBitmap(result);
			}
		}

	}

}
