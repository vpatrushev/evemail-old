package ru.micropint.evemail.appengine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import android.content.Context;
import android.util.Log;

import ru.micropint.evemail.data.EveAccount;
import ru.micropint.evemail.data.RawMail;
import ru.micropint.evemail.xml.CharacterNamesHandler;
import ru.micropint.evemail.xml.MailBodiesHandler;
import ru.micropint.evemail.xml.MailHeadersHandler;

public class MailLoader {

	private static final String TAG = "MailLoader";
	private static Object mLoadSyncObject = new Object();

	private EveAccount mAccount;
	private MailProviderUtils mProvider;
	private long mCacheExpiryDate;
	private int mNewMessageCount;

	public MailLoader(EveAccount account, Context c) {
		mAccount = account;
		mProvider = new MailProviderUtils(c.getContentResolver());
	}

	/**
	 * ��������� ����� � ���� ������
	 * 
	 * @throws IOException
	 *             ���� ���� ������ ����������
	 * @throws EveApiException
	 *             ���� ������ ������ �������
	 */
	public void load() throws IOException, EveApiException {
		MailHeadersHandler handler = new MailHeadersHandler();
		ArrayList<RawMail> loadedMail;
		synchronized (mLoadSyncObject) {
			Downloader.downloadAndParseXml(Constants.API_URL + "char/MailMessages.xml.aspx?characterID="
					+ mAccount.getCharacter().getId(), mAccount.getKey(), handler);
			loadedMail = handler.getData();
			Log.d(TAG, "Got " + loadedMail.size() + " new mails");
			removeExistingMails(loadedMail);
			Log.d(TAG, "After removing old mails " + loadedMail.size() + " left");
			loadMailBodies(loadedMail);
			removeEmptyMails(loadedMail);
			loadPortraits(loadedMail);
			checkForNewCharacters(loadedMail);
			mProvider.insertMail(loadedMail, mAccount.getCharacter().getId());
		}
		mCacheExpiryDate = handler.getCachedUntil();
		mNewMessageCount = loadedMail.size();
	}

	private void removeExistingMails(List<RawMail> mail) {
		int i = 0;
		int characterId = mAccount.getCharacter().getId();
		while (i < mail.size()) {
			if (mProvider.isMailExists(mail.get(i).getId(), characterId)) {
				mail.remove(i);
			} else {
				i++;
			}
		}
	}

	private void loadMailBodies(List<RawMail> mailList) throws IOException, EveApiException {
		if (!mailList.isEmpty()) {
			StringBuilder url = new StringBuilder(Constants.API_URL);
			url.append("char/MailBodies.xml.aspx?characterID=");
			url.append(mAccount.getCharacter().getId());
			url.append("&ids=");
			for (RawMail mail : mailList) {
				url.append(mail.getId());
				url.append(",");
			}
			url.deleteCharAt(url.length() - 1);

			MailBodiesHandler handler = new MailBodiesHandler(mailList);
			Downloader.downloadAndParseXml(url.toString(), mAccount.getKey(), handler);
		}
	}

	/**
	 * ������� ������ � ������� ������. ��� ������ API ������ ��������, �����
	 * ��������� ����, � ���� ���.
	 * 
	 * @param mail
	 *            ������ ���������� �����
	 */
	private void removeEmptyMails(ArrayList<RawMail> mail) {
		int i = 0;
		while (i < mail.size()) {
			if (mail.get(i).getBody() == null) {
				mail.remove(i);
			} else {
				i++;
			}
		}
	}

	private void loadPortraits(ArrayList<RawMail> mails) {
		for (RawMail mail : mails) {
			int senderId = mail.getSenderId();
			File portraitFile = new File(Utils.getPortraitFileName(senderId));
			if (!portraitFile.exists()) {
				Utils.downloadPortrait(senderId);
			}
		}
	}

	private void checkForNewCharacters(List<RawMail> mail) throws IOException, EveApiException {
		HashSet<Integer> newCharacters = new HashSet<Integer>();
		for (RawMail header : mail) {
			if (!mProvider.isCharacterExists(header.getSenderId())) {
				newCharacters.add(header.getSenderId());
			}
		}
		loadNewCharacters(newCharacters);
	}

	private void loadNewCharacters(Collection<Integer> ids) throws IOException, EveApiException {
		if (!ids.isEmpty()) {
			StringBuilder url = new StringBuilder(Constants.API_URL);
			url.append("eve/CharacterName.xml.aspx?ids=");
			for (Integer characterId : ids) {
				url.append(characterId);
				url.append(",");
			}
			url.deleteCharAt(url.length() - 1);

			CharacterNamesHandler handler = new CharacterNamesHandler();
			Downloader.downloadAndParseXml(url.toString(), mAccount.getKey(), handler);
			mProvider.insertCharacters(handler.getData());
		}
	}

	public long getCacheExpiryDate() {
		return mCacheExpiryDate;
	}

	public int getNewMessageCount() {
		return mNewMessageCount;
	}

}
