package ru.micropint.evemail.ui;

import java.util.List;

import ru.micropint.evemail.R;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class PreferencesActivity extends PreferenceActivity {
	
	private boolean mOptionsMenuVisible = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.preference_headers, target);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = new MenuInflater(this);
		inflater.inflate(R.menu.preferences_menu, menu);
		return true;
	};
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return mOptionsMenuVisible;
	};
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				setResult(Activity.RESULT_OK);
				finish();
				return true;
			case R.id.add_account:
				startActivity(new Intent(this, AddAccountActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public boolean isOptionsMenuVisible() {
		return mOptionsMenuVisible;
	}

	public void setOptionsMenuVisible(boolean visible) {
		this.mOptionsMenuVisible = visible;
	}
	
}
