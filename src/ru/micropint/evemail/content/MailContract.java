package ru.micropint.evemail.content;

import android.net.Uri;
import android.provider.BaseColumns;

public final class MailContract {

	public static final String AUTHORITY = "ru.micropint.evemail.MailProvider";

	public static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);

	public static final class Mail implements BaseColumns {

		static final String BASE_PATH = "mail";

		public static final String FORCE_ALL = "all";
		public static final String NEW_PATH = "/new";
		public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);

		public static final String MAIL_ID = MailTable.COLUMN_NAME_ID;
		public static final String SENDER_ID = MailTable.COLUMN_NAME_SENDER_ID;
		public static final String SENDER_NAME = CharactersTable.COLUMN_NAME_NAME;
		public static final String SENT_DATE = MailTable.COLUMN_NAME_SENT_DATE;
		public static final String TITLE = MailTable.COLUMN_NAME_TITLE;
		public static final String BODY = MailTable.COLUMN_NAME_BODY;
		public static final String TO_CORP_OR_ALLIANCE_ID = MailTable.COLUMN_NAME_TO_CORP_OR_ALLIANCE_ID;
		public static final String IS_READ = MailTable.COLUMN_NAME_IS_READ;
		public static final String IS_DELETED = MailTable.COLUMN_NAME_IS_DELETED;
		public static final String ACCOUNT_ID = MailTable.COLUMN_NAME_ACCOUNT_ID;
	}

	public static final class Character implements BaseColumns {
		
		static final String BASE_PATH = "character";

		public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
		
		public static final String NAME = CharactersTable.COLUMN_NAME_NAME;
	}

}
