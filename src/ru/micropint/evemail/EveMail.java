package ru.micropint.evemail;

import android.app.Application;

public class EveMail extends Application {

	private static EveMail instance;
	
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
	}

	public static EveMail getInstance() {
		return instance;
	}
	
}
