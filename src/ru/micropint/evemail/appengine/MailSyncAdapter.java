package ru.micropint.evemail.appengine;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

import ru.micropint.evemail.data.EveAccount;
import ru.micropint.evemail.data.EveCharacter;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

public class MailSyncAdapter extends AbstractThreadedSyncAdapter {

	private static final String TAG = "MailSyncAdapter";
	// ����������� ����� �� ��������� �������������
	private static final long SYNC_TIMEOUT = 60 * 1000;

	AccountManager mAccountManager;

	public MailSyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
		mAccountManager = AccountManager.get(context);
	}

	@Override
	public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider,
			SyncResult syncResult) {
		EveAccount eveAccount = new EveAccount(mAccountManager, account);
		MailLoader loader = new MailLoader(eveAccount, getContext());
		Log.i(TAG, "Syncing " + eveAccount.getCharacter().getName());
		try {
			loader.load();
			if (loader.getNewMessageCount() > 0) {
				sendNewMailBroadcast(eveAccount.getCharacter());
			}
			scheduleNextSync(account, eveAccount, loader);
		} catch (IOException e) {
			syncResult.stats.numIoExceptions++;
			e.printStackTrace();
		} catch (EveApiException e) {
			syncResult.stats.numParseExceptions++;
			e.printStackTrace();
		}
	}

	private void sendNewMailBroadcast(EveCharacter character) {
		Intent broadcast = new Intent(Constants.ACTION_NEW_MESSAGE);
		broadcast.putExtra(Constants.EXTRA_CHARACTER, character);
		getContext().sendOrderedBroadcast(broadcast, null);
	}

	private void setSyncAlarm(Account account, EveCharacter character, long time) {
		AlarmManager manager = (AlarmManager) getContext().getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(getContext(), MailSyncScheduler.class);
		intent.putExtra(MailSyncScheduler.EXTRA_ACCOUNT, account);
		PendingIntent pIntent = PendingIntent.getBroadcast(getContext(), character.getId(), intent,
				PendingIntent.FLAG_CANCEL_CURRENT);
		manager.set(AlarmManager.RTC_WAKEUP, time, pIntent);
	}

	private void scheduleNextSync(Account account, EveAccount eveAccount, MailLoader loader) {
		long nextSyncByServer = loader.getCacheExpiryDate() + 60 * 1000;
		long nextSyncByMinimumTimeout = System.currentTimeMillis() + SYNC_TIMEOUT;
		long nextSync = Math.max(nextSyncByServer, nextSyncByMinimumTimeout);
		setSyncAlarm(account, eveAccount.getCharacter(), nextSync);
		Log.d(TAG, "Scheduled sync at " + DateFormat.getDateTimeInstance().format(new Date(nextSync)));
	}

}
