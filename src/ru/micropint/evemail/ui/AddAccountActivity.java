package ru.micropint.evemail.ui;

import java.io.IOException;
import java.util.ArrayList;

import ru.micropint.evemail.R;
import ru.micropint.evemail.appengine.Constants;
import ru.micropint.evemail.appengine.DetachableAsyncTask;
import ru.micropint.evemail.appengine.Downloader;
import ru.micropint.evemail.appengine.EveApiException;
import ru.micropint.evemail.appengine.Utils;
import ru.micropint.evemail.content.MailContract;
import ru.micropint.evemail.data.ApiKey;
import ru.micropint.evemail.data.EveAccount;
import ru.micropint.evemail.data.EveCharacter;
import ru.micropint.evemail.xml.CharactersHandler;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class AddAccountActivity extends BaseActivity {

	public static final String ACTION_LOGIN = "ru.micropint.evemail.LOGIN";

	private static final String STATE_KEY_ID = "evemail.key_id";
	private static final String STATE_VERIFICATION_CODE = "evemail.verification_code";
	private static final String STATE_CHARACTERS = "evemail.characters";
	private static final String STATE_SELECTED_CHARACTER = "evemail.selected_character";

	private EditText mKeyIdView;
	private EditText mVerificationCodeView;
	private Button mLoadCharactersButton;
	private TextView mCharactersLabel;
	private RadioGroup mCharactersRadios;
	private Button mAddAccountButton;

	private ArrayList<EveCharacter> mCharacters;
	private LoadCharactersTask mLoadTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.add_account);

		mKeyIdView = (EditText) findViewById(R.id.KeyId);
		mVerificationCodeView = (EditText) findViewById(R.id.VerificationCode);
		mLoadCharactersButton = (Button) findViewById(R.id.LoadCharsButton);
		mCharactersLabel = (TextView) findViewById(R.id.Characters);
		mCharactersRadios = (RadioGroup) findViewById(R.id.CharacterRadios);
		mAddAccountButton = (Button) findViewById(R.id.AddButton);

		if (savedInstanceState == null) {
			setInitialState();
		} else {
			restoreState(savedInstanceState);
		}

		// listener ����������� ���������, ����� �� �� ���������� ��� ��������
		// ������
		new Handler().post(new Runnable() {
			public void run() {
				mKeyIdView.addTextChangedListener(keyEditChanged);
				mVerificationCodeView.addTextChangedListener(keyEditChanged);
			}
		});

		mLoadTask = (LoadCharactersTask) getLastNonConfigurationInstance();
		if (mLoadTask != null) {
			showWaitDialog(R.string.addAccount_CharactersLoading);
			mLoadTask.attach(this);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (intent.getAction() == Intent.ACTION_VIEW) {
			Uri data = intent.getData();
			mKeyIdView.setText(data.getQueryParameter("keyID"));
			mVerificationCodeView.setText(data.getQueryParameter("vCode"));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(STATE_KEY_ID, mKeyIdView.getText().toString());
		outState.putString(STATE_VERIFICATION_CODE, mVerificationCodeView.getText().toString());
		if (mCharacters != null && mCharacters.size() > 0) {
			outState.putSerializable(STATE_CHARACTERS, mCharacters);
			outState.putInt(STATE_SELECTED_CHARACTER, mCharactersRadios.getCheckedRadioButtonId());
		}
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		if (mLoadTask != null && mLoadTask.getStatus() != AsyncTask.Status.FINISHED) {
			mLoadTask.detach();
			Object result = mLoadTask;
			mLoadTask = null;
			return result;
		} else {
			return null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mLoadTask != null && mLoadTask.getStatus() != AsyncTask.Status.FINISHED) {
			mLoadTask.cancel(true);
			mLoadTask = null;
		}
		hideWaitDialog();
	}

	private void setInitialState() {
		mLoadCharactersButton.setEnabled(false);
		mCharactersLabel.setVisibility(View.GONE);
		mCharactersRadios.removeAllViews();
		mAddAccountButton.setEnabled(false);
	}

	@SuppressWarnings("unchecked")
	private void restoreState(Bundle state) {
		mKeyIdView.setText(state.getString(STATE_KEY_ID));
		mVerificationCodeView.setText(state.getString(STATE_VERIFICATION_CODE));
		mLoadCharactersButton.setEnabled(mKeyIdView.getText().length() > 0
				&& mVerificationCodeView.getText().length() > 0);
		mCharacters = (ArrayList<EveCharacter>) state.getSerializable(STATE_CHARACTERS);
		if (mCharacters != null) {
			showCharacters();
			int selectedCharId = state.getInt(STATE_SELECTED_CHARACTER);
			if (selectedCharId >= 0) {
				mCharactersRadios.check(selectedCharId);
			}
			mAddAccountButton.setEnabled(selectedCharId >= 0);
		} else {
			mCharactersLabel.setVisibility(View.GONE);
			mAddAccountButton.setEnabled(false);
		}
	}

	private void showCharacters() {
		int i = 0;
		for (EveCharacter character : mCharacters) {
			RadioButton button = new RadioButton(this);
			button.setId(i++);
			button.setText(character.getName());
			button.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					mAddAccountButton.setEnabled(true);
				}
			});
			mCharactersRadios.addView(button);
		}
	}

	public void openApiPage_Click(View v) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("https://support.eveonline.com/api/Key/activateinstalllinks"));
		startActivity(browserIntent);
	}

	public void createKey_Click(View v) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("https://support.eveonline.com/api/Key/CreatePredefined/3584"));
		startActivity(browserIntent);
	}

	public void addAccountClick(View v) {
		EveCharacter selectedCharacter = mCharacters.get(mCharactersRadios.getCheckedRadioButtonId());
		ApiKey key = new ApiKey(mKeyIdView.getText().toString(), mVerificationCodeView.getText().toString());
		EveAccount eveAccount = new EveAccount(key, selectedCharacter);

		Account account = new Account(selectedCharacter.getName(), Constants.ACCOUNT_TYPE);
		AccountManager manager = AccountManager.get(this);
		Bundle accountData = new Bundle();
		eveAccount.writeToStringBundle(accountData);
		boolean accountCreated = manager.addAccountExplicitly(account, "", accountData);

		if (accountCreated) {
			Intent intent = new Intent(this, InboxActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			intent.putExtra(InboxActivity.EXTRA_CHARACTER_ID, selectedCharacter.getId());
			startActivity(intent);
			ContentResolver.setSyncAutomatically(account, MailContract.AUTHORITY, true);
		} else {
			Toast.makeText(this, R.string.addAccount_Duplicate, Toast.LENGTH_SHORT).show();
		}
	}

	public void loadCharactersClick(View v) {
		Utils.hideKeyboard(this, mKeyIdView);
		Utils.hideKeyboard(this, mVerificationCodeView);
		mLoadTask = new LoadCharactersTask();
		mLoadTask.attach(this);
		mLoadTask.execute();
	}

	private TextWatcher keyEditChanged = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
			mLoadCharactersButton.setEnabled(mKeyIdView.length() > 0 && mVerificationCodeView.length() > 0);
			mCharactersLabel.setVisibility(View.GONE);
			mCharactersRadios.removeAllViews();
			mAddAccountButton.setEnabled(false);
		}
	};

	private static class LoadCharactersTask extends DetachableAsyncTask<AddAccountActivity, Void, Void, String> {

		@Override
		protected void onPreExecute() {
			mAttachedActivity.showWaitDialog(R.string.addAccount_CharactersLoading);
			mAttachedActivity.mCharactersRadios.removeAllViews();
			mAttachedActivity.mCharactersRadios.check(-1);
			mAttachedActivity.mAddAccountButton.setEnabled(false);
		}

		@Override
		protected String doInBackground(Void... params) {
			CharactersHandler handler = new CharactersHandler();
			ApiKey key = new ApiKey(mAttachedActivity.mKeyIdView.getText().toString().trim(),
					mAttachedActivity.mVerificationCodeView.getText().toString().trim());
			try {
				Downloader.downloadAndParseXml(Constants.API_URL + "account/Characters.xml.aspx", key, handler);
				mAttachedActivity.mCharacters = handler.getData();
				return null;
			} catch (IOException e) {
				return mAttachedActivity.getResources().getString(R.string.download_error);
			} catch (EveApiException e) {
				return e.getMessage();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			mAttachedActivity.mLoadTask = null;
			mAttachedActivity.hideWaitDialog();
			if (result == null) {
				mAttachedActivity.showCharacters();
			} else {
				Toast.makeText(mAttachedActivity, result, Toast.LENGTH_LONG).show();
			}
		}

	}

}
