package ru.micropint.evemail.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import ru.micropint.evemail.data.EveCharacter;

public class CharactersHandler extends BaseListHandler<EveCharacter> {

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		if (localName.equals("row")) {
			EveCharacter character = new EveCharacter();
			character.setId(Integer.parseInt(attributes.getValue("characterID")));
			character.setName(attributes.getValue("name"));
			character.setCorporationName(attributes.getValue("corporationName"));
			mData.add(character);
		}
	}

}
