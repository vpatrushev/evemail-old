package ru.micropint.evemail.appengine;

public class EveApiException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3602089040999728261L;
	private String message;
	
	public EveApiException() {
		message = "";
	}
	
	public EveApiException(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
}
