package ru.micropint.evemail.appengine;

import java.io.File;

import ru.micropint.evemail.R;
import ru.micropint.evemail.content.MailContract;
import ru.micropint.evemail.data.EveCharacter;
import ru.micropint.evemail.ui.InboxActivity;
import ru.micropint.evemail.ui.ReadMailActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

public class NewMailBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (!isNotificationsEnabled(context)) {
			return;
		}

		EveCharacter character = (EveCharacter) intent.getSerializableExtra(Constants.EXTRA_CHARACTER);
		String[] projection = new String[] {
				MailContract.Mail._ID,
				MailContract.Mail.SENDER_ID,
				MailContract.Mail.SENDER_NAME,
				MailContract.Mail.TITLE,
				MailContract.Mail.SENT_DATE,
				MailContract.Mail.BODY };
		String characterId = Integer.toString(character.getId());
		Cursor cur = context.getContentResolver().query(
				MailContract.Mail.CONTENT_URI,
				projection,
				MailContract.Mail.ACCOUNT_ID + "=? AND " + MailContract.Mail.IS_READ + "=0 AND "
						+ MailContract.Mail.SENDER_ID + " !=?",
				new String[] { characterId, characterId },
				MailContract.Mail.SENT_DATE + " DESC");

		if (cur.getCount() > 1) {
			NotificationCompat.Builder normal = new NotificationCompat.Builder(context);
			applyPreferences(context, normal);
			normal.setAutoCancel(true);
			normal.setContentInfo(Integer.toString(cur.getCount()));
			normal.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.stat_notify_email));
			normal.setSmallIcon(R.drawable.stat_notify_email);
			if (cur.getCount() == 1) {
				buildSingleMailNotification(context, character, cur, normal);
			}
			if (cur.getCount() > 1) {
				buildMultipleMailsNotification(context, character, cur, normal);
			}

			NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			manager.notify(character.getId(), normal.build());
		}
	}

	private boolean isNotificationsEnabled(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getBoolean(context.getString(R.string.prefKey_ShowNotifications), true);
	}

	private void buildSingleMailNotification(Context context, EveCharacter character, Cursor cur,
			NotificationCompat.Builder normal) {
		cur.moveToFirst();
		int senderId = cur.getInt(cur.getColumnIndexOrThrow(MailContract.Mail.SENDER_ID));
		String senderName = cur.getString(cur.getColumnIndexOrThrow(MailContract.Mail.SENDER_NAME));
		String mailTitle = cur.getString(cur.getColumnIndexOrThrow(MailContract.Mail.TITLE));
		CharSequence mailBody = Html.fromHtml(cur.getString(cur.getColumnIndexOrThrow(MailContract.Mail.BODY)));

		Intent openInboxIntent = new Intent(context, InboxActivity.class)
				.putExtra(InboxActivity.EXTRA_CHARACTER_ID, character.getId());
		Intent readMailIntent = new Intent(context, ReadMailActivity.class);
		readMailIntent.putExtra(ReadMailActivity.EXTRA_MAIL_ID,
				cur.getLong(cur.getColumnIndexOrThrow(MailContract.Mail._ID)));

		normal.setContentTitle(senderName)
				.setContentText(mailTitle)
				.setStyle(new NotificationCompat.BigTextStyle()
						.bigText(mailBody)
						.setSummaryText(getCharacterFullName(character)))
				.setContentIntent(TaskStackBuilder.create(context)
						.addNextIntent(openInboxIntent)
						.addNextIntent(readMailIntent)
						.getPendingIntent(0, PendingIntent.FLAG_CANCEL_CURRENT));

		File portraitFile = new File(Utils.getPortraitFileName(senderId));
		if (portraitFile.exists()) {
			normal.setLargeIcon(BitmapFactory.decodeFile(portraitFile.getAbsolutePath()));
		}
	}

	private void buildMultipleMailsNotification(Context context, EveCharacter character, Cursor cur,
			NotificationCompat.Builder normal) {
		int mailTitleColor = context.getResources().getColor(R.color.notification_secondary);
		Intent openInboxIntent = new Intent(context, InboxActivity.class)
				.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
				.putExtra(InboxActivity.EXTRA_CHARACTER_ID, character.getId());

		normal.setContentTitle(context.getString(R.string.notification_NewMails_Title))
				.setContentText(getCharacterFullName(character))
				.setContentIntent(
						PendingIntent.getActivity(context, 0, openInboxIntent, PendingIntent.FLAG_CANCEL_CURRENT));

		NotificationCompat.InboxStyle inbox = new NotificationCompat.InboxStyle();
		cur.moveToFirst();
		do {
			SpannableString mailInfo = formatMailInfo(cur, mailTitleColor);
			inbox.addLine(mailInfo);
		} while (cur.moveToNext());
		inbox.setSummaryText(getCharacterFullName(character));
		normal.setStyle(inbox);
	}

	private void applyPreferences(Context context, NotificationCompat.Builder builder) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int defaults = 0;

		String ringtonePrefKey = context.getString(R.string.prefKey_Ringtone);
		if (prefs.contains(ringtonePrefKey)) {
			builder.setSound(Uri.parse(prefs.getString(ringtonePrefKey, "")));
		} else {
			defaults |= Notification.DEFAULT_SOUND;
		}

		if (Utils.getDefaultPreferenceBoolean(R.string.prefKey_Vibrate, false)) {
			defaults |= Notification.DEFAULT_VIBRATE;
		}
		builder.setDefaults(defaults);
	}

	private SpannableString formatMailInfo(Cursor cur, int mailTitleColor) {
		String senderName = cur.getString(cur.getColumnIndexOrThrow(MailContract.Mail.SENDER_NAME));
		String mailTitle = cur.getString(cur.getColumnIndexOrThrow(MailContract.Mail.TITLE));
		SpannableString mailInfo = new SpannableString(senderName + ": " + mailTitle);
		mailInfo.setSpan(new ForegroundColorSpan(Color.WHITE), 0, senderName.length() + 2, 0);
		mailInfo.setSpan(new ForegroundColorSpan(mailTitleColor), senderName.length() + 2, mailInfo.length(), 0);
		return mailInfo;
	}

	private CharSequence getCharacterFullName(EveCharacter character) {
		return character.getName() + " (" + character.getCorporationName() + ")";
	}

}
