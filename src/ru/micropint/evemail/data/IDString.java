package ru.micropint.evemail.data;

import java.io.Serializable;

public class IDString implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2057871750941765342L;
	private int id;
	private String name;
	
	public IDString() {
		
	}
	
	public IDString(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
