package ru.micropint.evemail.adapters;

import java.util.List;

import ru.micropint.evemail.R;
import ru.micropint.evemail.appengine.Constants;
import ru.micropint.evemail.content.MailContract;
import ru.micropint.evemail.data.EveAccount;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class InboxNavigationAdapter extends ArrayAdapter<EveAccount> implements LoaderCallbacks<Cursor> {

	private LayoutInflater mInflater;
	private LoaderManager mLoaderManager;
	private int mLoaderTaskId;
	private SparseIntArray mUnreadCounts = new SparseIntArray();

	public InboxNavigationAdapter(Context context, List<EveAccount> objects, LoaderManager loaderManager,
			int loaderTaskId) {
		super(context, 0, objects);
		mInflater = LayoutInflater.from(context);
		mLoaderManager = loaderManager;
		mLoaderTaskId = loaderTaskId;

		mLoaderManager.initLoader(loaderTaskId, null, this);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View result = getDropDownView(position, convertView, parent);
		result.setPadding(result.getPaddingLeft(), 0, result.getPaddingRight(), 0);
		return result;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.navigation_account_item, null);
			ViewHolder holder = new ViewHolder();
			holder.account = (TextView) convertView.findViewById(R.id.account);
			holder.corporation = (TextView) convertView.findViewById(R.id.corporation);
			holder.unreadCount = (TextView) convertView.findViewById(R.id.unread_count);
			convertView.setTag(holder);
		}

		ViewHolder holder = (ViewHolder) convertView.getTag();
		EveAccount account = getItem(position);

		holder.account.setText(account.getCharacter().getName());
		holder.corporation.setText(account.getCharacter().getCorporationName());
		Integer unreadCount = mUnreadCounts.get(account.getCharacter().getId(), 0);
		if (unreadCount > 0) {
			holder.unreadCount.setText(unreadCount.toString());
		} else {
			holder.unreadCount.setText("");
		}

		return convertView;	}

	private static class ViewHolder {
		TextView account;
		TextView corporation;
		TextView unreadCount;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		String[] projection = {
				MailContract.Mail.ACCOUNT_ID,
				MailContract.Mail._COUNT
		};

		Uri.Builder builder = MailContract.Mail.CONTENT_URI.buildUpon();
		builder.appendQueryParameter(Constants.CONTENT_GROUP_BY, MailContract.Mail.ACCOUNT_ID);
		builder.appendQueryParameter(Constants.CONTENT_HAVING, MailContract.Mail.SENDER_ID + "!=" + MailContract.Mail.ACCOUNT_ID);

		return new CursorLoader(getContext(), builder.build(), projection,
				MailContract.Mail.IS_READ + "=?", new String[] { "0" }, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		if (loader.getId() == mLoaderTaskId) {
			mUnreadCounts = new SparseIntArray(cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					mUnreadCounts.append(cursor.getInt(0), cursor.getInt(1));
				} while (cursor.moveToNext());
			}
			notifyDataSetChanged();
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		if (loader.getId() == mLoaderTaskId) {
			mUnreadCounts = new SparseIntArray();
		}
	}

}
