package ru.micropint.evemail.xml;

import java.util.HashMap;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import ru.micropint.evemail.data.RawMail;

public class MailBodiesHandler extends BaseHandler {

	private static final String LINK_START_PATTERN = "<a href=";
	private static final String LINK_END_PATTERN = "</a>";
	private static final String COLOR_PATTERN = "color=\"#";
	private static final String FONT_SIZE_PATTERN = "<font size=\"";
	private static final int FONT_SIZE_DIVIDER = 4;
	
	private List<RawMail> mMailList;
	private HashMap<Integer, Integer> mMailIdPositionMap;
	private int mCurrentMailId;
	
	public MailBodiesHandler(List<RawMail> mailList) {
		mMailList = mailList;
		mMailIdPositionMap = new HashMap<Integer, Integer>(mailList.size());
		for (int i = 0; i < mMailList.size(); i++) {
			mMailIdPositionMap.put(mMailList.get(i).getId(), i);
		}
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		if (localName.equals("row")) {
			mCurrentMailId = Integer.parseInt(attributes.getValue("messageID"));
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("row")) {
			int mailPosition = mMailIdPositionMap.get(mCurrentMailId);
			mMailList.get(mailPosition).setBody(formatMailBody(mTagText));
		}
	}
	
	private String formatMailBody(String rawBody) {
		StringBuilder result = new StringBuilder(rawBody);
		removeEveLinks(result);
		fixFontColors(result);
		scaleFontSize(result);
		addDefaultFont(result);
		encodeBogusCharacters(result);
		return result.toString();
	}
	
	private void removeEveLinks(StringBuilder data) {
		// link starts
		int linkStartIndex = data.indexOf(LINK_START_PATTERN);
		while (linkStartIndex >= 0) {
			int linkEndIndex = data.indexOf(">", linkStartIndex + LINK_START_PATTERN.length());
			data.delete(linkStartIndex, linkEndIndex + 1);
			linkStartIndex = data.indexOf(LINK_START_PATTERN);
		}
		//link ends
		linkStartIndex = data.indexOf(LINK_END_PATTERN);
		while (linkStartIndex >= 0) {
			data.delete(linkStartIndex, linkStartIndex + LINK_END_PATTERN.length());
			linkStartIndex = data.indexOf(LINK_END_PATTERN);
		}
	}
	
	private void fixFontColors(StringBuilder data) {
		int buggedColorPos = indexOfBuggedColor(data);
		while (buggedColorPos >= 0) {
			int alpha = Integer.parseInt(data.substring(buggedColorPos, buggedColorPos + 2), 16);
			int red = Integer.parseInt(data.substring(buggedColorPos + 2, buggedColorPos + 4), 16);
			int green = Integer.parseInt(data.substring(buggedColorPos + 4, buggedColorPos + 6), 16);
			int blue = Integer.parseInt(data.substring(buggedColorPos + 6, buggedColorPos + 8), 16);
			String correctColor = Integer.toHexString(red * alpha / 256) + Integer.toHexString(green * alpha / 256) + Integer.toHexString(blue * alpha / 256);
			data.replace(buggedColorPos, buggedColorPos + 8, correctColor);
			buggedColorPos = indexOfBuggedColor(data);
		}
	}
	
	private int indexOfBuggedColor(StringBuilder data) {
		int startPos = data.indexOf(COLOR_PATTERN);
		int shift = COLOR_PATTERN.length();
		while (startPos >= 0) {
			int endPos = data.indexOf("\">", startPos + shift);
			if (endPos - startPos == shift + 8) {
				return startPos + shift;
			}
			startPos = data.indexOf(COLOR_PATTERN, endPos);
		}
		return -1;
	}
	
	private void scaleFontSize(StringBuilder data) {
		int startIndex = data.indexOf(FONT_SIZE_PATTERN);
		int shift = FONT_SIZE_PATTERN.length();
		while (startIndex >= 0) {
			int endIndex = data.indexOf("\"", startIndex + shift);
			int fontSize = Integer.parseInt(data.substring(startIndex + shift, endIndex));
			data.replace(startIndex + shift, endIndex, Integer.toString(fontSize / FONT_SIZE_DIVIDER));
			startIndex = data.indexOf(FONT_SIZE_PATTERN, endIndex);
		}
	}
	
	private void addDefaultFont(StringBuilder data) {
		if (data.indexOf(FONT_SIZE_PATTERN) == -1) {
			data.insert(0, "<font size=\"3\" color=\"#c9c9c9\">");
			data.append("</font>");
		}
	}
	
	private void encodeBogusCharacters(StringBuilder data) {
		int pos = data.indexOf("%");
		while (pos >= 0) {
			data.delete(pos, pos + 1);
			data.insert(pos, "&#37;");
			pos = data.indexOf("%");
		}
	}
	
}
