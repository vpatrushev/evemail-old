package ru.micropint.evemail.appengine;

import android.app.Activity;
import android.os.AsyncTask;

public abstract class DetachableAsyncTask<ActivityClass extends Activity, Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

	protected ActivityClass mAttachedActivity;

	/**
	 * ����������� ������ � Activity. ������ ���� ��������� ����� ��������
	 * ������ ��� ��� �������������� Activity ��� ����� ������������
	 * 
	 * @param a
	 *            Activity, � ������� ���� ���������� ������
	 */
	public void attach(ActivityClass a) {
		mAttachedActivity = a;
	}

	/**
	 * ���������� ������ �� Activity. ������ ���������� � Activity.onDestroy
	 */
	public void detach() {
		mAttachedActivity = null;
	}
}
