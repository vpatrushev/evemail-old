package ru.micropint.evemail.ui;

import ru.micropint.evemail.R;
import ru.micropint.evemail.appengine.Constants;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;

public class BaseActivity extends Activity {
	
	private ProgressDialog mProgressDlg;

	/**
	 * ���������� ������ ��������
	 */
	public void showWaitDialog() {
		if (mProgressDlg == null) {
			try {
				if (getParent() == null) {
					mProgressDlg = ProgressDialog.show(this, "", getResources().getString(R.string.wait), true);
				} else {
					mProgressDlg = ProgressDialog.show(this.getParent(), "", getResources().getString(R.string.wait), true);
				}
			} catch (IllegalArgumentException e) {
				Log.d(Constants.DEBUG_TAG, "IllegalArgument when showing wait dialog: " + e.getLocalizedMessage());
			}
		}
	}

	/**
	 * ���������� ������ ��������
	 * 
	 * @param resId
	 *            ������ �� ��������� ������ � �������, ������� ����� ������� �
	 *            �������
	 */
	public void showWaitDialog(int resId) {
		if (mProgressDlg == null) {
			try {
				mProgressDlg = ProgressDialog.show(this, "", getResources().getString(resId), true);
			} catch (IllegalArgumentException e) {
				Log.d(Constants.DEBUG_TAG, "IllegalArgument when showing wait dialog: " + e.getLocalizedMessage());
			}
		}
	}

	/**
	 * ������� ���������� ����� ������ ��������
	 */
	public void hideWaitDialog() {
		if (mProgressDlg != null) {
			try {
				mProgressDlg.dismiss();
				mProgressDlg = null;
			} catch (IllegalArgumentException e) {
				Log.d(Constants.DEBUG_TAG, "IllegalArgument when hiding wait dialog: " + e.getLocalizedMessage());
			}
			mProgressDlg = null;
		}
	}

}
