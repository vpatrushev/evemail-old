package ru.micropint.evemail.appengine;

import ru.micropint.evemail.content.MailContract;
import android.accounts.Account;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MailSyncScheduler extends BroadcastReceiver {

	public static final String EXTRA_ACCOUNT = "account";

	@Override
	public void onReceive(Context context, Intent intent) {
		Account account = (Account) intent.getParcelableExtra(EXTRA_ACCOUNT);
		Log.d("MailSyncScheduler", "Received sync broadcast for " + account.name);
		ContentResolver.requestSync(account, MailContract.AUTHORITY, new Bundle());
	}

}
