package ru.micropint.evemail.data;

import java.io.Serializable;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;


public class EveAccount implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1689563680457957657L;
	private ApiKey mKey;
	private EveCharacter mCharacter;
	private Account mAccount;
	
	public EveAccount() {
		mKey = new ApiKey();
		mCharacter = new EveCharacter();
	}
	
	public EveAccount(ApiKey key, EveCharacter character) {
		super();
		this.mKey = key;
		this.mCharacter = character;
	}
	
	public EveAccount(AccountManager manager, Account account) {
		mKey = new ApiKey(manager, account);
		mCharacter = new EveCharacter(manager, account);
		mAccount = account;
	}
	
	public void writeToStringBundle(Bundle bundle) {
		mKey.writeToStringBundle(bundle);
		mCharacter.writeToStringBundle(bundle);
	}

	public EveCharacter getCharacter() {
		return mCharacter;
	}
	public void setCharacter(EveCharacter character) {
		this.mCharacter = character;
	}
	public ApiKey getKey() {
		return mKey;
	}
	public void setKey(ApiKey key) {
		this.mKey = key;
	}

	public Account getAccount() {
	    return mAccount;
    }

	public void setAccount(Account mAccount) {
	    this.mAccount = mAccount;
    }

}
