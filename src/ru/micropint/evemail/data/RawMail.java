package ru.micropint.evemail.data;

import java.util.Date;

public class RawMail {
	
	private int id;
	private int senderId;
	private Date sentDate;
	private String title;
	private String characterIds;
	private int corpOrAllianceId;
	private int listId;
	private String body;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSenderId() {
		return senderId;
	}
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	public Date getSentDate() {
		return sentDate;
	}
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getCorpOrAllianceId() {
		return corpOrAllianceId;
	}
	public void setCorpOrAllianceId(int corpOrAllianceId) {
		this.corpOrAllianceId = corpOrAllianceId;
	}
	public int getListId() {
		return listId;
	}
	public void setListId(int listId) {
		this.listId = listId;
	}
	public String getCharacterId() {
		return characterIds;
	}
	public void setCharacterId(String characterIds) {
		this.characterIds = characterIds;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}	
	
}
