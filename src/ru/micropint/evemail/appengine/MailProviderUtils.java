package ru.micropint.evemail.appengine;

import java.util.AbstractCollection;

import ru.micropint.evemail.content.MailContract;
import ru.micropint.evemail.data.IDString;
import ru.micropint.evemail.data.RawMail;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

public class MailProviderUtils {

	private ContentResolver mResolver;

	public MailProviderUtils(ContentResolver resolver) {
		mResolver = resolver;
	}

	/**
	 * �������� ��� ��������� ��� �����������
	 * 
	 * @param characterId
	 *            ID ��������� � Eve Online
	 */
	public void markAllMessagesRead(int characterId) {
		ContentValues values = new ContentValues();
		values.put(MailContract.Mail.IS_READ, true);
		mResolver.update(
				MailContract.Mail.CONTENT_URI,
				values,
				MailContract.Mail.ACCOUNT_ID + "=?",
				new String[] { Integer.toString(characterId) });
	}

	/**
	 * �������� ��������� ��� �����������
	 * 
	 * @param id
	 *            ID ������ � ��
	 */
	public void markMessageRead(long id) {
		setMessageReadState(id, true);
	}

	/**
	 * �������� ��������� ��� �������������
	 * 
	 * @param messageId
	 *            ID ������ � ��
	 */
	public void markMessageUnread(long id) {
		setMessageReadState(id, false);
	}

	/**
	 * ������� ���������
	 * 
	 * @param messageId
	 *            ID ������ � ��
	 */
	public void delete(long id) {
		mResolver.delete(ContentUris.withAppendedId(MailContract.Mail.CONTENT_URI, id), null, null);
	}

	/**
	 * ������� ��� ���������
	 * 
	 * @param characterId
	 *            ID ��������� � Eve Online
	 */
	public void clean(int characterId) {
		mResolver.delete(MailContract.Mail.CONTENT_URI,
				MailContract.Mail.ACCOUNT_ID + "=?",
				new String[] { Integer.toString(characterId) });
	}

	/**
	 * ��������� ���������
	 * 
	 * @param mails
	 *            ������ ���������
	 * @param characterId
	 *            ID ��������� � Eve Online
	 */
	public void insertMail(AbstractCollection<RawMail> mails, int characterId) {
		for (RawMail mail : mails) {
			ContentValues values = new ContentValues();
			values.put(MailContract.Mail.MAIL_ID, mail.getId());
			values.put(MailContract.Mail.TITLE, mail.getTitle());
			values.put(MailContract.Mail.BODY, mail.getBody());
			values.put(MailContract.Mail.SENDER_ID, mail.getSenderId());
			values.put(MailContract.Mail.SENT_DATE, mail.getSentDate().getTime());
			values.put(MailContract.Mail.TO_CORP_OR_ALLIANCE_ID, mail.getCorpOrAllianceId());
			values.put(MailContract.Mail.ACCOUNT_ID, characterId);
			values.put(MailContract.Mail.IS_READ, false);
			mResolver.insert(MailContract.Mail.CONTENT_URI, values);
		}
	}

	/**
	 * ���������, ���������� �� ������ � ������ ����������. ��������� �����
	 * ���������, ������� ���� ������� � �������.
	 * 
	 * @param id
	 *            ID ���������
	 * @param characterId
	 *            ID ��������� � Eve Online
	 * @return
	 */
	public boolean isMailExists(int id, int characterId) {
		Uri uri = MailContract.Mail.CONTENT_URI.buildUpon().appendQueryParameter(MailContract.Mail.FORCE_ALL, "1").build();
		Cursor cur = mResolver.query(
				uri,
				new String[] { MailContract.Mail._ID },
				MailContract.Mail.MAIL_ID + "=? AND " + MailContract.Mail.ACCOUNT_ID + "=?",
				new String[] { Integer.toString(id), Integer.toString(characterId) },
				null);
		boolean result = cur.getCount() > 0;
		cur.close();
		return result;
	}

	public void insertCharacters(AbstractCollection<IDString> characters) {
		for (IDString character : characters) {
			ContentValues values = new ContentValues();
			values.put(MailContract.Character._ID, character.getId());
			values.put(MailContract.Character.NAME, character.getName());
			mResolver.insert(MailContract.Character.CONTENT_URI, values);
		}
	}

	public boolean isCharacterExists(int id) {
		Uri uri = ContentUris.withAppendedId(MailContract.Character.CONTENT_URI, id);
		Cursor cur = mResolver.query(uri, new String[] { MailContract.Character._ID }, null, null, null);
		boolean result = cur.getCount() > 0;
		cur.close();
		return result;
	}

	private void setMessageReadState(long id, boolean isRead) {
		ContentValues values = new ContentValues();
		values.put(MailContract.Mail.IS_READ, isRead);
		mResolver.update(ContentUris.withAppendedId(MailContract.Mail.CONTENT_URI, id), values, null, null);
	}

}
