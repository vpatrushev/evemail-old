package ru.micropint.evemail.appengine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.xml.sax.SAXException;

import ru.micropint.evemail.data.ApiKey;
import ru.micropint.evemail.xml.BaseHandler;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.http.AndroidHttpClient;
import android.util.Log;

public class Downloader {

	public static URLConnection openConnection(String url) throws IOException {
		Log.d(Constants.SERVER_DEBUG_TAG, url);
		URL request = new URL(url);
		URLConnection conn = request.openConnection();
		conn.connect();
		return conn;
	}

	public static InputStream download(String url) throws IOException {
		return openConnection(url).getInputStream();
	}

	public static void downloadAndParseXml(String url, ApiKey key, BaseHandler xmlHandler) throws IOException, EveApiException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser;
		try {
			parser = factory.newSAXParser();
			parser.parse(download(addAuthInfo(url, key)), xmlHandler);
			if (xmlHandler.hasError()) {
				throw new EveApiException(xmlHandler.getError());
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}

	public static String copyStreamToString(InputStream inputStream) {
		BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
		StringBuilder total = new StringBuilder();
		String line;
		try {
			while ((line = r.readLine()) != null) {
				total.append(line);
			}
		} catch (IOException e) {
			return "";
		}
		return total.toString();
	}

	public static Drawable downloadImage(String url) throws IOException {
		return Drawable.createFromStream(download(url), "src");
	}
	
	public static Bitmap downloadBitmap(String url) {
	    final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
	    final HttpGet getRequest = new HttpGet(url);

	    try {
	        HttpResponse response = client.execute(getRequest);
	        final int statusCode = response.getStatusLine().getStatusCode();
	        if (statusCode != HttpStatus.SC_OK) { 
	            Log.w("ImageDownloader", "Error " + statusCode + " while retrieving bitmap from " + url); 
	            return null;
	        }
	        
	        final HttpEntity entity = response.getEntity();
	        if (entity != null) {
	            InputStream inputStream = null;
	            try {
	                inputStream = entity.getContent(); 
	                final Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
	                return bitmap;
	            } finally {
	                if (inputStream != null) {
	                    inputStream.close();  
	                }
	                entity.consumeContent();
	            }
	        }
	    } catch (Exception e) {
	        // Could provide a more explicit error message for IOException or IllegalStateException
	        getRequest.abort();
	        Log.w("ImageDownloader", "Error while retrieving bitmap from " + url, e);
	    } finally {
	        if (client != null) {
	            client.close();
	        }
	    }
	    return null;
	}

	private static String addAuthInfo(String url, ApiKey key) {
		StringBuilder result = new StringBuilder(url);
		if (url.contains("?")) {
			result.append("&");
		} else {
			result.append("?");
		}
		result.append("keyID=");
		result.append(key.getKeyId());
		result.append("&vCode=");
		result.append(key.getVerificationCode());
		return result.toString();
	}

}
