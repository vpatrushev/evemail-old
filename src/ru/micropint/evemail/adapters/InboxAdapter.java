package ru.micropint.evemail.adapters;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ru.micropint.evemail.EveMail;
import ru.micropint.evemail.R;
import ru.micropint.evemail.appengine.Utils;
import ru.micropint.evemail.content.MailContract;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class InboxAdapter extends CursorAdapter {

	private static final DateFormat SAME_DAY_DATE_FORMAT = DateFormat.getTimeInstance(DateFormat.SHORT);
	private static final DateFormat SAME_YEAR_DATE_FORMAT = DateFormat.getDateInstance(DateFormat.SHORT);

	private LayoutInflater mInflater;
	private ListView mListView;
	private int mTitleIndex;
	private int mSenderNameIndex;
	private int mSentDateIndex;
	private int mIsReadIndex;
	private boolean mConvertTime;

	public InboxAdapter(Context context, ListView  listView) {
		super(context, null, 0);
		mListView = listView;
		mInflater = LayoutInflater.from(context);
		updateConvertTimePreference();
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();

		holder.checkbox.setTag(cursor.getPosition());
		holder.checkbox.setOnCheckedChangeListener(null);
		holder.checkbox.setChecked(mListView.isItemChecked(cursor.getPosition()));
		holder.checkbox.setOnCheckedChangeListener(checkbox_checked_change);
		holder.title.setText(cursor.getString(mTitleIndex));
		holder.senderName.setText(cursor.getString(mSenderNameIndex));

		boolean isRead = cursor.getInt(mIsReadIndex) == 0;
		long sentUnixTime = cursor.getLong(mSentDateIndex);
		GregorianCalendar sentDate = new GregorianCalendar();
		sentDate.setTimeInMillis(sentUnixTime);
		GregorianCalendar now = new GregorianCalendar();
		Utils.setTimezone(sentDate, mConvertTime);
		Utils.setTimezone(now, mConvertTime);
		if (isSameDay(sentDate, now)) {
			holder.sentDate.setText(SAME_DAY_DATE_FORMAT.format(new Date(sentUnixTime)));
		} else if (!isSameYear(sentDate, now)) {
			holder.sentDate.setText(SAME_YEAR_DATE_FORMAT.format(new Date(sentUnixTime)));
		} else {
			holder.sentDate.setText(sentDate.get(Calendar.DAY_OF_MONTH) + " "
					+ EveMail.getInstance().getResources().getStringArray(R.array.months_short)[sentDate.get(Calendar.MONTH)]);
		}

		holder.isRead = isRead;
		if (isRead) {
			holder.title.setTypeface(null, Typeface.BOLD);
			holder.senderName.setTypeface(null, Typeface.BOLD);
		} else {
			holder.title.setTypeface(null, Typeface.NORMAL);
			holder.senderName.setTypeface(null, Typeface.NORMAL);
		}
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View result = mInflater.inflate(R.layout.listitem_mail_header, null);

		ViewHolder holder = new ViewHolder();
		holder.checkbox = (CheckBox) result.findViewById(R.id.checkbox);
		holder.title = (TextView) result.findViewById(R.id.Title);
		holder.senderName = (TextView) result.findViewById(R.id.SenderName);
		holder.sentDate = (TextView) result.findViewById(R.id.SentDate);
		result.setTag(holder);

		return result;
	}

	@Override
	public Cursor swapCursor(Cursor newCursor) {
		if (newCursor != null) {
			mTitleIndex = newCursor.getColumnIndexOrThrow(MailContract.Mail.TITLE);
			mSenderNameIndex = newCursor.getColumnIndexOrThrow(MailContract.Mail.SENDER_NAME);
			mSentDateIndex = newCursor.getColumnIndexOrThrow(MailContract.Mail.SENT_DATE);
			mIsReadIndex = newCursor.getColumnIndexOrThrow(MailContract.Mail.IS_READ);
		}
		return super.swapCursor(newCursor);
	};

	public void updateConvertTimePreference() {
		String convertTimePrefName = EveMail.getInstance().getString(R.string.prefKey_ConvertToLocalTime);
		mConvertTime = PreferenceManager.getDefaultSharedPreferences(EveMail.getInstance()).getBoolean(convertTimePrefName, false);
		Utils.setTimezone(SAME_DAY_DATE_FORMAT, mConvertTime);
		Utils.setTimezone(SAME_YEAR_DATE_FORMAT, mConvertTime);
	}

	private boolean isSameDay(GregorianCalendar date1, GregorianCalendar date2) {
		return date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR) && date1.get(Calendar.DAY_OF_YEAR) == date2.get(Calendar.DAY_OF_YEAR);
	}

	private boolean isSameYear(GregorianCalendar date1, GregorianCalendar date2) {
		return date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR);
	}
	
	private OnCheckedChangeListener checkbox_checked_change = new OnCheckedChangeListener() {
		
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			int position = (Integer) buttonView.getTag();
			mListView.setItemChecked(position, isChecked);
		}
	};

	public static class ViewHolder {
		public boolean isRead;
		public CheckBox checkbox;
		public TextView title;
		public TextView senderName;
		public TextView sentDate;
	}

}
