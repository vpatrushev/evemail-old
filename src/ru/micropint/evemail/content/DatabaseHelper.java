package ru.micropint.evemail.content;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

class DatabaseHelper extends SQLiteOpenHelper {
	
	static final String TAG = "MailDatabase";
	static final String DATABASE_NAME = "eve_mail.db";
	static final int DATABASE_VERSION = 2;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(TAG, "Creating database");
		db.execSQL("CREATE TABLE " + MailTable.TABLE_NAME + " ("
				+ BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ MailTable.COLUMN_NAME_ID + " INTEGER NOT NULL,"
				+ MailTable.COLUMN_NAME_TITLE + " TEXT,"
				+ MailTable.COLUMN_NAME_BODY + " TEXT,"
				+ MailTable.COLUMN_NAME_SENDER_ID + " INTEGER,"
				+ MailTable.COLUMN_NAME_SENT_DATE + " DATE,"
				+ MailTable.COLUMN_NAME_TO_CORP_OR_ALLIANCE_ID + " INTEGER,"
				+ MailTable.COLUMN_NAME_TO_CHARACTER_IDS + " TEXT,"
				+ MailTable.COLUMN_NAME_TO_LIST_ID + " INTEGER,"
				+ MailTable.COLUMN_NAME_ACCOUNT_ID + " INTEGER NOT NULL,"
				+ MailTable.COLUMN_NAME_IS_READ + " INTEGER,"
				+ MailTable.COLUMN_NAME_IS_DELETED + " INTEGER,"
				+ MailTable.COLUMN_NAME_IS_NEW + " INTEGER"
				+ ");");
		db.execSQL("CREATE INDEX mail_read_idx ON " + MailTable.TABLE_NAME + "(" + MailTable.COLUMN_NAME_IS_READ + ")");
		db.execSQL("CREATE INDEX mail_new_idx ON " + MailTable.TABLE_NAME + "(" + MailTable.COLUMN_NAME_IS_NEW + ")");

		db.execSQL("CREATE TABLE " + CharactersTable.TABLE_NAME + " ("
				+ CharactersTable.COLUMN_NAME_ID + " INTEGER PRIMARY KEY,"
				+ CharactersTable.COLUMN_NAME_NAME + " TEXT"
				+ ");");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if (oldVersion == 1) {
			db.execSQL("DROP TABLE " + MailTable.TABLE_NAME);
			db.execSQL("DROP TABLE " + CharactersTable.TABLE_NAME);
			onCreate(db);
		}
	}

}